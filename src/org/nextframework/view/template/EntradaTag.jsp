<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="tldsgp" uri="sgp"%>

<c:set var="userAlt"   value="${TEMPLATE_beanName}.userAlt"/>
<c:set var="timeAlt" value="${TEMPLATE_beanName}.timeAlt"/> 

<t:tela titulo="SGP <img src='/SGP/img/style/next.png'> ${complementoURL} <span class='titulo-pagina-corrente'>${entradaTag.titulo} ${complemento2URL} </span>">
		<c:if test="${consultar}">
			<input type="hidden" name="forcarConsulta" value="true"/>
		</c:if>
		<c:if test="${param.fromInsertOne == 'true'}">
			<input type="hidden" name="fromInsertOne" value="true"/>
		</c:if>

		<c:if test="${empty param.showMenu}">
			<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
				<div class="linkBar">
					${entradaTag.invokeLinkArea}			
					<c:if test="${entradaTag.showListagemLink}">
						<c:if test="${!consultar}">
							<n:link action="listagem" class="outterTableHeaderLink" onclick="return dialogVoltar();" >Listagem</n:link>
						</c:if>
						<c:if test="${consultar}">
							<n:link action="listagem" onclick="dialogAguarde();" class="outterTableHeaderLink">Listagem</n:link>
						</c:if>
					</c:if>				
				<br />
				<br />
				</div>
			</c:if>
		</c:if>
		<c:if test="${!empty param.showMenu}">
			<div align="right">
				<n:link onclick="javascript:window.parent.closeIframeWithoutReload();">
					<font style="font-size: 16px;">Fechar </font>
				</n:link>
			</div>
		</c:if>	
		<div>
			<n:bean name="${TEMPLATE_beanName}">
				<c:if test="${consultar}">
					<c:set var="modeConsultar" value="output" scope="request"/>
					<t:property name="${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}" mode="input" write="false"/>
				</c:if>
				<t:propertyConfig mode="${n:default('input', modeConsultar)}">
					<n:doBody />
				</t:propertyConfig>
			</n:bean>
		</div>
		<c:if test="${param.ACAO != 'criar' && param.ACAO != 'copiar'}">
			<div id="last-update">
				<font style="color:#999">
					<n:panel>�ltima altera��o neste registro foi feita por <font style="color:#E77272">${n:reevaluate(userAlt,pageContext)}</font> no dia <font style="color:#E77272">${tldsgp:dateformat(n:reevaluate(timeAlt,pageContext))}</font> �s <font style="color:#E77272">${tldsgp:hourformat(n:reevaluate(timeAlt,pageContext))}</font> </n:panel>
				</font>
			</div>
		</c:if>
</t:tela>


