package br.com.zatz.sgp.util;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;


public class DatabaseError {
	
	
	
	/**
	 * Verifica se o valor � Duplicado para determinado index, quando tem mais de um index na mesma tabela
	 * @param e DataIntegrityViolationException
	 * @param key {@link String} contendo a chave da constraint
	 * @return {@link Boolean} se existe o registro no banco ou n�o
	 */
	public static Boolean isKeyPresent(DataIntegrityViolationException e,String key) {
		ConstraintViolationException constraint = (ConstraintViolationException)e.getCause();

		SQLException sqlException = (SQLException)constraint.getSQLException();
		SQLException nextException = sqlException.getNextException();
		
		String message = nextException==null?sqlException.getMessage():nextException.getMessage();
		if (message != null && !"".equals(message)) message = message.toUpperCase();			
		if (key != null && !"".equals(key)) key = key.toUpperCase();
		
		String erroMySQLDuplicate = "Duplicate entry";
		
		boolean msgMysql = (message.contains(key) && message.contains(erroMySQLDuplicate.toUpperCase()));
		if(msgMysql)
			return true;
		else
			return message.contains(key);
	}
	
}	
