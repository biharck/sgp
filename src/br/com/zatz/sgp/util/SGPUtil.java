package br.com.zatz.sgp.util;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nextframework.core.web.NextWeb;

public class SGPUtil {
	
	public static String getLoginUsuarioLogado(){
		return NextWeb.getUser().getLogin();
	}
	
	/**
	 * <p>M�todo que devolve somente o id enviado de uma requisi��o junto com o endere�o do pacote
	 * @param values {@link String} contendo o id completo
	 * @return {@link Integer} id
	 */
	public static Integer returnOnlyId(String... values) {
		Pattern pattern = Pattern.compile("\\d");
		StringBuilder result = new StringBuilder();
		Matcher matcher = pattern.matcher(values[0]);
		while (matcher.find())
			result.append(matcher.group());
		return Integer.parseInt(result.toString());
	}
	
	/**
	 * Retorna a data formantada dd/MM/yyyy
	 * @param data
	 * @return String {@link DateFormat}
	 */
	public static String dateToString(Timestamp date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(date);
	}

	/**
	 * Retorna a data formantada dd/MM/yyyy
	 * @param data
	 * @return String {@link DateFormat}
	 */
	public static String dateToString(Date date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(date);
	}
	
	/**
	 * Retorna a hora formatada em HH:MM
	 * @param time
	 * @return String {@link DateFormat}
	 */
	public static String timeToString(Timestamp date){
		DateFormat dateFormat = new SimpleDateFormat("HH:MM");
		return	dateFormat.format(date);
	}
	
	public static void sendMailNewScheda(String assunto, String componentes, String projeto, String numero, 
			String vencimento, String cliente, String destinatario, String projectChief) throws IOException, Exception{
		
		//Prepara o envio para os projectChief do componente
		sendMail("templateNovaScheda.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"projeto:"+projeto,
				"componente:"+componentes,
				"vencimento:"+vencimento,
				"cliente:"+cliente				
				);
	}
	
	public static void sendMailInteracaoScheda(String assunto,String destinatario, String projectChief, 
			String numero, String componente, String projeto, String interacao, String autor) throws IOException, Exception{
		sendMail("templateInteracaoScheda.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"componente:"+componente,
				"projeto:"+projeto,
				"autor:"+autor,
				"interacao:"+interacao
				);
	}

	public static void sendMailFechamentoScheda(String assunto,String destinatario, String projectChief, 
			String numero, String componente, String projeto, String interacao, String autor, String investimento) throws IOException, Exception{
		sendMail("templateFechamentoScheda.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"componente:"+componente,
				"projeto:"+projeto,
				"autor:"+autor,
				"investimento:"+investimento,
				"interacao:"+interacao
				);
	}

	public static void sendMailReaberturaScheda(String assunto,String destinatario, String projectChief, 
			String numero, String componente, String projeto, String autor) throws IOException, Exception{
		sendMail("templateSchedaReaberta.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"componente:"+componente,
				"projeto:"+projeto,
				"autor:"+autor
				);
	}
	
	public static void sendMailNewDesvio(String assunto, String componentes, String projeto, String numero, 
			String denominacao, String cliente, String destinatario, String projectChief) throws IOException, Exception{
		
		//Prepara o envio para os projectChief do componente
		sendMail("templateNovoDesvio.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"projeto:"+projeto,
				"componente:"+componentes,
				"denominacao:"+denominacao,
				"cliente:"+cliente				
				);
	}
	
	public static void sendMailSchedasEmAberto(String assunto, String componentes, String projeto, String numero, 
			String vencimento, String cliente, String destinatario, String projectChief) throws IOException, Exception{
		
		//Prepara o envio para os projectChief do componente
		sendMail("templateSchedasEmAberto.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"projeto:"+projeto,
				"componente:"+componentes,
				"vencimento:"+vencimento,
				"cliente:"+cliente				
				);
	}
	
	public static void sendMailSchedasVencidas(String assunto, String componentes, String projeto, String numero, 
			String vencimento, String cliente, String destinatario, String projectChief) throws IOException, Exception{
		
		//Prepara o envio para os projectChief do componente
		sendMail("templateSchedasVencidas.tpl",assunto,destinatario,
				"projectChief:"+projectChief,
				"numero:"+numero,
				"projeto:"+projeto,
				"componente:"+componentes,
				"vencimento:"+vencimento,
				"cliente:"+cliente				
		);
	}
	
	private static void sendMail(String templateName,String assunto,String destinatario, String... param) throws IOException, Exception{
		TemplateManager template = new TemplateManager("WEB-INF/template/"+templateName);
		// adiciona as informa��es do email ao template
		try{
			for (int i = 0; i < param.length; i++) {
				template.assign(param[i].split(":")[0], param[i].split(":")[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		EmailUtil emailDepart = new EmailUtil("suporte@zatz.eng.br", "suporte@zatz.eng.br", 
				"abcd123.", "smtp.gmail.com", "587", "true", destinatario, assunto, "");
		emailDepart.addHtmlText(template.getTemplate());
		emailDepart.enviar();
		
	}
	
	/**
	 * <b>M�todo respons�vel em incrementar datas</b>
	 * 
	 * @param data - Data a ser incrementada.
	 * @param num - N�mero de vezes que um campo ser� incrementado.
	 * @param field - Campo a ser incrementado. <B>Exemplo:</b> <code>Calendar.DAY_OF_MONTH</code>
	 * @return java.sql.Date
	 * 
	 * @author Biharck
	 */
	public static java.sql.Date incrementDate(Date data, int num, int field){
		Calendar dt = Calendar.getInstance();
		dt.setTimeInMillis(data.getTime());
		dt.add(field, num);
		return new Date(dt.getTimeInMillis());
	}  
	
}
