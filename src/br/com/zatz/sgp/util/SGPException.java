package br.com.zatz.sgp.util;

public class SGPException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SGPException() {
		super();
	}

	public SGPException(String message, Throwable cause) {
		super(message, cause);
	}

	public SGPException(String message) {
		super(message);
	}

	public SGPException(Throwable cause) {
		super(cause);
	}

}
