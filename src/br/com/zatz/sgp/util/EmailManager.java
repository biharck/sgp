package br.com.zatz.sgp.util;



import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Class to send email with html body and attachs<br>
 * <br>
 * how to use:<br>
 *      String fileAttachment = "path/of/file";<br>
 *		String text = "<simg src='cid:00001'> example to show an image in email body";<br>
 *		<br>
 *		Email email = new Email("smtp","from@from.com","to@to.com","subject");<br>
 *		email.addHtmlText(text)<br>
 *			 .attachFile(fileAttachment, "name/of/file", "00001")<br>
 *			 .sendMessage();<br>
 *  <br>
 */
public class EmailManager {

	protected String smtp;
	private String springsmtp;
	protected String from;
	protected String to;
	protected String subject;
	protected Multipart multipart;
	protected MimeMessage message;
	private Boolean configured = false;
	protected List<String> listTo;
	protected Session session;

	public EmailManager setSmtp(String smtp) {
		this.smtp = smtp;
		return this;
	}
	
	public void setSpringsmtp(String springsmtp) {
		this.smtp = springsmtp;
	}
	
	public String getSpringsmtp() {
		return springsmtp;
	}

	public EmailManager setFrom(String from) {
		this.from = from;
		return this;
	}

	public EmailManager setTo(String to) {
		this.to = to;
		return this;
	}
	
	public EmailManager setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getFrom() {
		return from;
	}

	public MimeMessage getMessage() {
		return message;
	}

	public Multipart getMultipart() {
		return multipart;
	}

	public String getSmtp() {
		return smtp;
	}

	public String getSubject() {
		return subject;
	}

	public String getTo() {
		return to;
	}

	public EmailManager setMessage(MimeMessage message) {
		this.message = message;
		return this;
	}
	
	public List<String> getListTo() {
		return listTo;
	}
	
	public EmailManager setListTo(List<String> listTo) {
		this.listTo = listTo;
		return this;
	}

	public EmailManager setMultipart(Multipart multipart) {
		this.multipart = multipart;
		return this;
	}

	public EmailManager(String smtp,String from,String to,String subject) throws Exception {
		this.setSmtp(smtp);
		this.setFrom(from);
		this.setTo(to);
		this.setSubject(subject);
		this.configure();
	}
	
	public EmailManager() {
	}

	private EmailManager configure() throws Exception {
		verify();
		Properties props = System.getProperties();
		props.put("smtp.vitapres.com.br", smtp);
		
		session = Session.getInstance(props, null);
		message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		if (listTo != null) {
			for (String element : listTo) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(element));
			}
		} else {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		}
		message.setSubject(subject);
		multipart = new MimeMultipart();
		this.configured = true;
		return this;
	}

	public EmailManager attachFile(String file,String filename,String contentType, String id) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setHeader("Content-Type", contentType);
		messageBodyPart.setFileName(filename);
		messageBodyPart.setContentID("<"+id+">");
		addPart(messageBodyPart);
		return this;
	}
	
	private Boolean verify() {
		if (smtp == null) {throw new NullPointerException("smtp can�t be null");}
		else if (from == null) {throw new NullPointerException("from can�t be null");}
		else if (to == null && listTo == null) {throw new NullPointerException("to can�t be null");}
		else if (subject == null) {throw new NullPointerException("subject can�t be null");}
		return true;
	}

	
	public void addPart(MimeBodyPart part) throws Exception {
		if (verify() && !configured){configure();}
		multipart.addBodyPart(part);
	}
	
	public EmailManager addHtmlText(String text) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text);
		messageBodyPart.setHeader("Content-Type", "text/html");	
		addPart(messageBodyPart);
		return this;
	}
	
	public void sendMessage() throws Exception{
		message.setContent(multipart);
		Transport.send(message);
		this.setFrom(null);
		this.setTo(null);
		this.setListTo(null);
		this.setSubject(null);
		session = Session.getInstance(System.getProperties(), null);
		message = new MimeMessage(session);
	}
}
