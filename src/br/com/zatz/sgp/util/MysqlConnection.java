package br.com.zatz.sgp.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.nextframework.exception.NextException;

public class MysqlConnection {
	private Connection connection;

	private String username = "";
	private String password = "";
	private String url = "";

	public Connection getConnection() {
		return connection;
	}

	public MysqlConnection() {
		ResourceBundle bundle = null;
		try {
			bundle = ResourceBundle.getBundle("connection");
		} catch (MissingResourceException e) {
			// caso nao encontre o bundle nao fazer nada
		}
		if (bundle != null) {
			try {
				//driver, url, username, password
				url = bundle.getString("url");
				username = null;
				password = null;
				try {
					username = bundle.getString("username");
					password = bundle.getString("password");
				} catch (Exception e) {
					//o nome de usu�rio e o password sao opcionais
				}
			} catch (MissingResourceException e) {
				throw new NextException("Erro ao carregar informa��es de connection.properties. O arquivo est� incorreto, " +
						"faltando algum dos parametros: driver, url");
			} 
		}	
		
		try {
			// Load the JDBC driver
			String driverName = "com.mysql.jdbc.Driver";
			Class.forName(driverName);
			connection = DriverManager.getConnection(url, username, password);
			System.out.println("Conectou no banco mysql Support");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}