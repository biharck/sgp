package br.com.zatz.sgp.controller.process;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.web.servlet.ModelAndView;

import org.nextframework.authorization.User;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.bean.annotation.Bean;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.controller.OnErrors;
import org.nextframework.core.standard.Next;
import org.nextframework.core.web.WebRequestContext;

import br.com.zatz.sgp.adm.filtro.AlteraSenhaFiltro;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.service.UsuarioService;
import br.com.zatz.sgp.util.PathUtil;
import br.com.zatz.sgp.util.SGPException;

@Bean
@Controller(path="/adm/process/AlteraSenha",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AlteraSenhaProcess extends MultiActionController {

	@DefaultAction
	public ModelAndView doPage(WebRequestContext webRequestContext, AlteraSenhaFiltro filtro) throws Exception {
		return new ModelAndView("process/alteraSenha", "filtro", filtro);
	}

	public ModelAndView doSalvar(WebRequestContext webRequestContext, AlteraSenhaFiltro filtro) throws Exception {
		UsuarioService usuarioService = UsuarioService.getInstance();
		User user = webRequestContext.getUser();
		Usuario load = usuarioService.loadForEntrada((Usuario) user);
						
		String senhaAtual = filtro.getSenhaAtual();
		String senhaNova = filtro.getSenhaNova();
		String reSenhaNova = filtro.getReSenhaNova();
		
		if(!senhaNova.equals(reSenhaNova)){
			webRequestContext.addMessage("A nova senha e a confirma��o devem ser iguais.", MessageType.ERROR);
			return doPage(webRequestContext, filtro);
		}
	
		if(!usuarioService.isCheckPassword(senhaAtual, load.getSenha())){
			webRequestContext.addMessage("Sua senha atual n�o confere, digite outra senha.", MessageType.ERROR);
			return doPage(webRequestContext, filtro);
		}
		
		if(usuarioService.isSenhaAnterior(load, senhaNova)){
			webRequestContext.addMessage("Senha igual as 3 anteriores, digite outra senha", MessageType.ERROR);
			return doPage(webRequestContext, filtro);
		}
		
		//troca senhas
		load.setSenhaAnterior2(load.getSenhaAnterior());
		load.setSenhaAnterior(load.getSenha());
		
		
		load.setSenha(senhaNova);
		load.setReSenha(reSenhaNova);
		usuarioService.criptografaSenha(load);
		load.setSenhaProvisoria(false);
		
		usuarioService.alteraSenha(load);
		webRequestContext.addMessage("Senha alterada com sucesso.");
		webRequestContext.getServletRequest().getSession().setAttribute("isSenhaProvisoria", false);
		
		return new ModelAndView("redirect:"+PathUtil.AFTER_LOGIN_GO_TO);
	}
}
