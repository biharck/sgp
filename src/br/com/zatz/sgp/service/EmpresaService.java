package br.com.zatz.sgp.service;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Empresa;
import br.com.zatz.sgp.dao.EmpresaDAO;

public class EmpresaService extends GenericService<Empresa>{

	private EmpresaDAO empresaDAO;
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	
	public Empresa getEmpresa(){
		return empresaDAO.getEmpresa();
	}
}
