package br.com.zatz.sgp.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Cronograma30PassosComponente;
import br.com.zatz.sgp.dao.Cronograma30PassosComponenteDAO;

public class Cronograma30PassosComponenteService extends GenericService<Cronograma30PassosComponente>{

	private Cronograma30PassosComponenteDAO cronograma30PassosComponenteDAO;
	public void setCronograma30PassosComponenteDAO(Cronograma30PassosComponenteDAO cronograma30PassosComponenteDAO) {
		this.cronograma30PassosComponenteDAO = cronograma30PassosComponenteDAO;
	}
	
	public List<Cronograma30PassosComponente> getCronogramas(Componente componente){
		return cronograma30PassosComponenteDAO.getCronogramas(componente);
	}
}
