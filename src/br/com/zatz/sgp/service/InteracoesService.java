package br.com.zatz.sgp.service;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Interacoes;

public class InteracoesService extends GenericService<Interacoes> {
	
	
	private static InteracoesService instance;
	public static InteracoesService getInstance() {
		if(instance == null){
			instance = Next.getObject(InteracoesService.class);
		}
		return instance;
	}
	
	
}
