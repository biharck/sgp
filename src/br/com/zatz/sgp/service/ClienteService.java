package br.com.zatz.sgp.service;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Cliente;
import br.com.zatz.sgp.bean.Desvio;
import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.dao.ClienteDAO;

public class ClienteService extends GenericService<Cliente>{

	private ClienteDAO clienteDAO;
	public void setClienteDAO(ClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}
	
	public Cliente getClienteByScheda(Scheda scheda){
		return clienteDAO.getClienteByScheda(scheda);
	}
	public Cliente getClienteByDesvio(Desvio desvio){
		return clienteDAO.getClienteByDesvio(desvio);
	}
}
