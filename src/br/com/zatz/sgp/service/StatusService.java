package br.com.zatz.sgp.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Status;
import br.com.zatz.sgp.dao.StatusDAO;

public class StatusService extends GenericService<Status>{
	
	/**
	 *  M�todo que retorna uma lista de Status que um ticket pode obter
	 * @return List<Status>
	 * @see support.autorizacao.dao.StatusDAO#getStatusToSendEmail
	 */
	public List<Status> getStatusToSendEmail(){
		StatusDAO statusDAO = new StatusDAO();
		return statusDAO.getStatusToSendEmail();
	}
}
