package br.com.zatz.sgp.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.AnexosComponente;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.dao.AnexosComponenteDAO;

public class AnexosComponenteService extends GenericService<AnexosComponente> {

	private AnexosComponenteDAO anexosComponenteDAO;
	public void setAnexosComponenteDAO(AnexosComponenteDAO anexosComponenteDAO) {
		this.anexosComponenteDAO = anexosComponenteDAO;
	}
	
	public List<AnexosComponente> getAnexosComponentesByComponente(Componente componente){
		return anexosComponenteDAO.getAnexosComponentesByComponente(componente);
	}
}
