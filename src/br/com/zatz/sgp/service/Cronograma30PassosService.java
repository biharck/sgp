package br.com.zatz.sgp.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Cronograma30Passos;
import br.com.zatz.sgp.dao.Cronograma30PassosDAO;

public class Cronograma30PassosService extends GenericService<Cronograma30Passos> {

	private Cronograma30PassosDAO cronograma30PassosDAO;
	
	public void setCronograma30PassosDAO(Cronograma30PassosDAO cronograma30PassosDAO) {
		this.cronograma30PassosDAO = cronograma30PassosDAO;
	}
	
	public List<Cronograma30Passos> getCronogramas(){
		return cronograma30PassosDAO.getCronogramas();
	}
	
	public List<Cronograma30Passos> getCronogramasPai(){
		return cronograma30PassosDAO.getCronogramasPai();
	}
}
