package br.com.zatz.sgp.service;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Municipio;
import br.com.zatz.sgp.bean.Uf;
import br.com.zatz.sgp.dao.UfDAO;

public class UfService extends GenericService<Uf> {
	
	private UfDAO ufDAO;
	public void setUfDAO(UfDAO ufDAO) {
		this.ufDAO = ufDAO;
	}
	
	public Uf getUfByMunicipio(Municipio m){
		return ufDAO.getUfByMunicipio(m);
	}	
}
