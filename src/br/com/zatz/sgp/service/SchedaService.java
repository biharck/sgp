package br.com.zatz.sgp.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.nextframework.core.web.NextWeb;
import org.nextframework.service.GenericService;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Interacoes;
import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.bean.SchedaComponente;
import br.com.zatz.sgp.bean.Status;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.dao.SchedaDAO;
import br.com.zatz.sgp.util.SGPException;
import br.com.zatz.sgp.util.SGPUtil;

public class SchedaService extends GenericService<Scheda>{

	private TransactionTemplate transactionTemplate;
	private SchedaDAO schedaDAO;
	private SchedaComponenteService schedaComponenteService;
	private ClienteService clienteService;
	
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setSchedaDAO(SchedaDAO schedaDAO) {
		this.schedaDAO = schedaDAO;
	}
	public void setSchedaComponenteService(SchedaComponenteService schedaComponenteService) {
		this.schedaComponenteService = schedaComponenteService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	public void saveOrUpdate(final Scheda bean) {
		if(bean.getId()==null){
			//cria o scheda com o status solicitado
			bean.setStatus(new Status(1));
			//Criando uma �nica transa��o, caso falhe o registro n�o � salvo incompleto
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					Interacoes interacoes = new Interacoes();
					interacoes = bean.getInteracoesTransient();
					interacoes.setAnexos(bean.getInteracoesTransient().getAnexos());
					List<Interacoes> listaInteracoes = new ArrayList<Interacoes>();
					listaInteracoes.add(interacoes);
					bean.setInteracoes(listaInteracoes);
					List<SchedaComponente> schedasComponentes = new ArrayList<SchedaComponente>();
					if(bean.getDesenhos()!=null){
						for (Componente c : bean.getDesenhos()) {
							schedasComponentes.add(new SchedaComponente(bean,c));
						}
					}
					// salva o novo scheda no banco
					schedaDAO.saveOrUpdate(bean);
					for (SchedaComponente schedaComponente : schedasComponentes) {
						schedaComponenteService.saveOrUpdate(schedaComponente);
					}
					
					return null;	
				}
			});
			try {
				String dataVencimento = SGPUtil.dateToString(SGPUtil.incrementDate(bean.getDataSolicitacao(), 15, Calendar.DAY_OF_MONTH));
				Scheda s = new Scheda();
				s.setId(bean.getId());
				s = loadForEntrada(bean);
				String nomeProjeto = s.getSchedaComponente().get(0).getComponente().getProjeto().getNome();
				String numeroProjeto = s.getNumero();
				String cliente = clienteService.getClienteByScheda(s).getNomeFantasia();
				/**
				 * Otimizar o tempo de envio de e-mail
				 */
				for (SchedaComponente sc : s.getSchedaComponente()){
					SGPUtil.sendMailNewScheda("Nova Scheda n�mero "+ bean.getNumero() +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							nomeProjeto, 
							numeroProjeto, 
							dataVencimento, 
							cliente, 
							sc.getComponente().getProjectChief().getEmail(), 
							sc.getComponente().getProjectChief().getNome()
							);
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new SGPException("Ups! uma falha ocorreu ao tentar enviar um e-mail sobre uma nova Scheda!" +
						" Tente novamente mais tarde ou entre em contato com os administradores do Sistema. Obrigado! :) ");
			} 
		}else{
			String ultimaInteracao = bean.getInteracoesTransient().getDetalhes();
			String investimento = "";
			if(bean.getInteracoesTransient().getInvestimento()!=null)
				investimento = bean.getInteracoesTransient().getInvestimento().toString();
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					if(bean.getStatus().getId()==3){//fechamento da scheda
						if(bean.getInteracoesTransient().getInvestimento()==null)
							throw new SGPException("Quando uma Scheda � fechada, um valor de investimento deve ser lan�ado. Por favor, informe este valor!");
						
						bean.setInvestimento(bean.getInteracoesTransient().getInvestimento());
					}
					Scheda temp = loadForEntrada(bean);
					bean.setNumero(temp.getNumero());
					bean.setDataSolicitacao(temp.getDataSolicitacao());
					bean.setProjectChief(temp.getProjectChief());
					bean.setInvestimento(temp.getInvestimento());
					bean.setDescricao(temp.getDescricao());
					Interacoes interacao = new Interacoes();
					interacao = bean.getInteracoesTransient();
					interacao.setAnexos(bean.getInteracoesTransient().getAnexos());
					List<Interacoes> listaDeInteracoes = temp.getInteracoes();
					listaDeInteracoes.add(interacao);
					bean.setInteracoes(listaDeInteracoes);
					
					// salva a intera��o feita no banco
					schedaDAO.saveOrUpdate(bean);
					return null;	
				}
			});
			try {
				
				Scheda s = new Scheda();
				s.setId(bean.getId());
				s = loadForEntrada(bean);
				String nomeProjeto = s.getSchedaComponente().get(0).getComponente().getProjeto().getNome();
				String numeroProjeto = s.getNumero();
				if(bean.getStatus().getId() == 3){//scheda fechada
					enviaEmailFechamentoScheda(bean.getNumero(), ultimaInteracao, s.getSchedaComponente(), nomeProjeto, numeroProjeto,investimento);
				}else if(bean.getStatus().getId()==4){//scheda reaberta
					enviaEmailReaberturaScheda(bean.getNumero(),s.getSchedaComponente(), nomeProjeto, numeroProjeto);
				}else
					enviaEmailEdicaoScheda(bean.getNumero(), ultimaInteracao, s.getSchedaComponente(), nomeProjeto, numeroProjeto);
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new SGPException("Ups! uma falha ocorreu ao tentar enviar um e-mail sobre uma nova Scheda!" +
						" Tente novamente mais tarde ou entre em contato com os administradores do Sistema. Obrigado! :) ");
			} 
		}
	}
	private void enviaEmailEdicaoScheda(String numeroScheda,
			String ultimaInteracao, List<SchedaComponente> schedasComponente, String nomeProjeto,
			String numeroProjeto) throws IOException, Exception {
		/**
		 * Otimizar o tempo de envio de e-mail
		 */
		for (SchedaComponente sc : schedasComponente){
			SGPUtil.sendMailInteracaoScheda("Altera��o da Scheda n�mero " + numeroScheda +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					sc.getComponente().getProjectChief().getEmail(), 
					sc.getComponente().getProjectChief().getNome(), 
					numeroProjeto,
					"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					nomeProjeto,
					ultimaInteracao,
					((Usuario)NextWeb.getUser()).getNome()
					);
			
		}
	}
	private void enviaEmailFechamentoScheda(String numeroScheda,
			String ultimaInteracao, List<SchedaComponente> schedasComponente, String nomeProjeto,
			String numeroProjeto, String investimento) throws IOException, Exception {
		/**
		 * Otimizar o tempo de envio de e-mail
		 */
		for (SchedaComponente sc : schedasComponente){
			SGPUtil.sendMailFechamentoScheda("Altera��o da Scheda n�mero " + numeroScheda +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					sc.getComponente().getProjectChief().getEmail(), 
					sc.getComponente().getProjectChief().getNome(), 
					numeroProjeto,
					"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					nomeProjeto,
					ultimaInteracao,
					((Usuario)NextWeb.getUser()).getNome(),
					investimento
					);
			
		}
	}
	
	public void enviaEmailReaberturaScheda(String numeroScheda,
			 List<SchedaComponente> schedasComponente, String nomeProjeto,
			String numeroProjeto) throws IOException, Exception {
		/**
		 * Otimizar o tempo de envio de e-mail
		 */
		for (SchedaComponente sc : schedasComponente){
			SGPUtil.sendMailReaberturaScheda("Altera��o da Scheda n�mero " + numeroScheda +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					sc.getComponente().getProjectChief().getEmail(), 
					sc.getComponente().getProjectChief().getNome(), 
					numeroProjeto,
					"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
					nomeProjeto,
					((Usuario)NextWeb.getUser()).getNome()
					);
			
		}
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Schedas de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @see br.com.zatz.sgp.dao.SchedaDAO#getSchedasByComponente
	 * @return {@link List} {@link Scheda}
	 */
	public List<Scheda> getSchedasByComponente(String idComponentes){
		return schedaDAO.getSchedasByComponente(idComponentes);
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Schedas n�o fechadas de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @see br.com.zatz.sgp.dao.SchedaDAO#getSchedasByComponente
	 * @return {@link List} {@link Scheda}
	 */
	public List<Scheda> getSchedasNaoFechadasByComponente(String idComponentes){
		return schedaDAO.getSchedasNaoFechadasByComponente(idComponentes);
	}
	
	/**
	 * <p>M�todo respons�vel em atualizar o status de uma determinada Scheda</p>
	 * @param statusId {@link Integer}
	 */
	public void updateStatusScheda(Integer idScheda, Integer statusId){
		schedaDAO.updateStatusScheda(idScheda,statusId);
	}

	
	public List<Scheda> getSchedasFaltandoMenos5Dias(){
		return schedaDAO.getSchedasFaltandoMenos5Dias();
	}
	
	public List<Scheda> getSchedasVencidas(){
		return schedaDAO.getSchedasVencidas();
	}
	
	public List<Scheda> getSchedasByIds(String ids){
		return schedaDAO.getSchedasByIds(ids);
	}
	
}


