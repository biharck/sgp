package br.com.zatz.sgp.service;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Cronograma30Passos;
import br.com.zatz.sgp.bean.Cronograma30PassosComponente;
import br.com.zatz.sgp.dao.ComponenteDAO;

public class ComponenteService extends GenericService<Componente>{

	private Cronograma30PassosService cronograma30PassosService;
	private ComponenteDAO componenteDAO;
	
	public void setCronograma30PassosService(Cronograma30PassosService cronograma30PassosService) {
		this.cronograma30PassosService = cronograma30PassosService;
	}
	public void setComponenteDAO(ComponenteDAO componenteDAO) {
		this.componenteDAO = componenteDAO;
	}
	
	@Override
	public void saveOrUpdate(Componente bean) {
		if(bean!=null && bean.getId()!=null){
			//testar se o tamanho do arquivo � maior que 16M ou o tamanho da vari�vel max_allowed_packet
			
			Componente temp = new Componente();
			temp.setId(bean.getId());
			temp = load(temp);
			bean.setProjeto(temp.getProjeto());
			bean.setVdVt(temp.getVdVt());
		}else if(bean.isCopia()){
			Componente temp = new Componente();
			temp.setId(bean.getVdVt().getId());
			temp = load(temp);
			bean.setProjeto(temp.getProjeto());
			bean.setMatricula(temp.getMatricula());
			bean.setNome(temp.getNome());
		}else if(bean.getId()==null){//registro novo
			List<Cronograma30Passos> cronogramas = cronograma30PassosService.getCronogramas();
			List<Cronograma30PassosComponente> listaCronograma30PassosComponente = new ArrayList<Cronograma30PassosComponente>();
			for (Cronograma30Passos c30P : cronogramas) {
				Cronograma30PassosComponente c30pc = new Cronograma30PassosComponente();
				c30pc.setComponente(bean);
				c30pc.setCronograma30Passos(c30P);
				listaCronograma30PassosComponente.add(c30pc);
			}
			bean.setListaCronograma30PassosComponente(listaCronograma30PassosComponente);
		}
		super.saveOrUpdate(bean);
	}
	
	public Componente getComponenteById(Integer id){
		return componenteDAO.getComponenteById(id);
	}
}
