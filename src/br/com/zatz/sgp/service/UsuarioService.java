package br.com.zatz.sgp.service;

import java.sql.Timestamp;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.dao.UsuarioDAO;
import br.com.zatz.sgp.util.SGPException;

public class UsuarioService extends GenericService<Usuario> {
		
	private Usuario userEncrypt;
	private UsuarioDAO usuarioDAO;
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	private static UsuarioService instance;
	public static UsuarioService getInstance() {
		if(instance == null){
			instance = Next.getObject(UsuarioService.class);
		}
		return instance;
	}
	
	private void encryptPassword(Usuario bean){
		userEncrypt = bean;
		if(userEncrypt.getSenha().equals(userEncrypt.getReSenha())){
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			userEncrypt.setSenha(encryptor.encryptPassword(bean.getSenha()));
		}else
			throw new SGPException("A senha e a confirmação devem ser iguais.");
	}
	
	public void criptografaSenha(Usuario bean){
		encryptPassword(bean);
	}
	
	public boolean isCheckPassword(String senha,String senhaEncrytor ){
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		return encryptor.checkPassword(senha, senhaEncrytor);
	}
	
	public boolean isSenhaAnterior(Usuario bean, String newSenha){
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		if(encryptor.checkPassword(newSenha,bean.getSenha()) || 
				encryptor.checkPassword(newSenha,bean.getSenhaAnterior()) ||
				encryptor.checkPassword(newSenha,bean.getSenhaAnterior2()))
			return true;
		else
			return false;
	}
	
	public void alteraSenha(Usuario bean){
		usuarioDAO.alteraSenha(bean);
	}
	
	public void alteraDtUltimoLogin(Usuario bean){
		bean.setDtUltimoLogin(new Timestamp(System.currentTimeMillis()));
		usuarioDAO.alteraDtUltimoLogin(bean);
	}
	
	
	
}
