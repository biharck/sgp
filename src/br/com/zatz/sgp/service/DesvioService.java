package br.com.zatz.sgp.service;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.service.GenericService;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Desvio;
import br.com.zatz.sgp.bean.DesvioComponente;
import br.com.zatz.sgp.dao.DesvioDAO;
import br.com.zatz.sgp.util.SGPException;
import br.com.zatz.sgp.util.SGPUtil;

public class DesvioService extends GenericService<Desvio>{
	
	private TransactionTemplate transactionTemplate;
	private DesvioDAO desvioDAO;
	private ClienteService clienteService;
	private ComponenteService componenteService;
	
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setDesvioDAO(DesvioDAO desvioDAO) {
		this.desvioDAO = desvioDAO;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setComponenteService(ComponenteService componenteService) {
		this.componenteService = componenteService;
	}
	
	
	@Override
	public void saveOrUpdate(final Desvio bean) {
		if(bean.getId()==null){
			//Criando uma �nica transa��o, caso falhe o registro n�o � salvo incompleto
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					List<DesvioComponente> desviosComponentes = new ArrayList<DesvioComponente>();
					if(bean.getDesenhos()!=null){
						for (Componente c : bean.getDesenhos()) {
							desviosComponentes.add(new DesvioComponente(bean,c));
						}
					}	
					bean.setDesvioComponente(desviosComponentes);
					desvioDAO.saveOrUpdate(bean);
					enviaEmailNovoDesvio(bean);
					return null;	
				}
			});
		}else{
			
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					Desvio temp = loadForEntrada(bean);
					bean.setDesvioComponente(temp.getDesvioComponente());
					desvioDAO.saveOrUpdate(bean);
					return null;	
				}
			});			
		}
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Desvios de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @see br.com.zatz.sgp.dao.DesvioDAO#getDesviosByComponente
	 * @return {@link List} {@link Desvio}
	 */
	public List<Desvio> getDesviosByComponente(String idComponentes){
		return desvioDAO.getDesviosByComponente(idComponentes);
	}
	
	public void enviaEmailNovoDesvio(Desvio desvio){
		
		try {
			Desvio d = loadForEntrada(desvio);
			
			String denominacao = d.getDenominacao();
			String nomeProjeto = componenteService.getComponenteById(d.getDesvioComponente().get(0).getComponente().getId()).getProjeto().getNome();
			String numeroProjeto = d.getNumero();
			String cliente = clienteService.getClienteByDesvio(d).getNomeFantasia();
			/**
			 * Otimizar o tempo de envio de e-mail
			 */
			for (DesvioComponente dc : d.getDesvioComponente()){
				Componente temp = componenteService.getComponenteById(dc.getComponente().getId());
				SGPUtil.sendMailNewDesvio("Novo Desvio n�mero "+ d.getNumero() +" para o componente #"+temp.getNumero() + " - " +temp.getNome(), 
						"#"+temp.getNumero() + " - " +temp.getNome(), 
						nomeProjeto, 
						numeroProjeto, 
						denominacao, 
						cliente, 
						temp.getProjectChief().getEmail(), 
						temp.getProjectChief().getNome()
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SGPException("Ups! uma falha ocorreu ao tentar enviar um e-mail sobre um novo Desvio!" +
					" Tente novamente mais tarde ou entre em contato com os administradores do Sistema. Obrigado! :) ");
		} 
	}
	
	public void enviaEmailAlteracaoDesvio(){
		
	}
}
