package br.com.zatz.sgp.adm.filtro;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.types.Cnpj;

public class ClienteFiltro extends FiltroListagem {

	private String nomeFantasia;
	private Cnpj cnpj;

	@DisplayName("Nome")
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public Cnpj getCnpj() {
		return cnpj;
	}
	
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
}
