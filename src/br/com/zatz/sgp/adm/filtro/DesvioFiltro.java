package br.com.zatz.sgp.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Projeto;

public class DesvioFiltro extends FiltroListagem {
	
	private String numero;
	private Projeto projeto;
	private Componente componente;
	private String denominacao;
	
	public String getNumero() {
		return numero;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Componente getComponente() {
		return componente;
	}
	public String getDenominacao() {
		return denominacao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
	public void setDenominacao(String denominacao) {
		this.denominacao = denominacao;
	}
}
