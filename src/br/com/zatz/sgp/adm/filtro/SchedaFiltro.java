package br.com.zatz.sgp.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.MaxLength;

import br.com.zatz.sgp.bean.Status;

public class SchedaFiltro extends FiltroListagem {
	
	protected String descricao;
	protected Status Status;

	public Status getStatus() {
		return Status;
	}

	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setStatus(Status status) {
		Status = status;
	}
	
}
