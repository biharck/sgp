package br.com.zatz.sgp.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.bean.Cliente;

public class ProjetoFiltro extends FiltroListagem {

	private String nome;
	private Cliente cliente;
	private String identificacao;
	
	public String getNome() {
		return nome;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
}
