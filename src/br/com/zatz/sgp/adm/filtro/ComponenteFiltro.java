package br.com.zatz.sgp.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.bean.Projeto;
import br.com.zatz.sgp.bean.Usuario;

public class ComponenteFiltro extends FiltroListagem {

	private String nome;
	private Projeto projeto;
	private String numero;
	private String matricula;
	private String numeroTCAE;
	private Usuario projectChief;
	
	public String getNome() {
		return nome;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public String getNumero() {
		return numero;
	}
	public String getMatricula() {
		return matricula;
	}
	public String getNumeroTCAE() {
		return numeroTCAE;
	}
	public Usuario getProjectChief() {
		return projectChief;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setNumeroTCAE(String numeroTCAE) {
		this.numeroTCAE = numeroTCAE;
	}
	public void setProjectChief(Usuario projectChief) {
		this.projectChief = projectChief;
	}
}
