package br.com.zatz.sgp.adm.filtro;

import java.util.List;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.bean.EnumAtivo;
import br.com.zatz.sgp.bean.Papel;

public class UsuarioFiltro extends FiltroListagem {

	private String nome;
	private EnumAtivo ativo;
	private List<Papel> papeis;
	
	public String getNome() {
        return nome;
    }
	@DisplayName("Status")
	public EnumAtivo getAtivo() {
		return ativo;
	}
	public List<Papel> getPapeis() {
		return papeis;
	}

    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setAtivo(EnumAtivo ativo) {
		this.ativo = ativo;
	}
    public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
}
