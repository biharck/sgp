package br.com.zatz.sgp.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.bean.Cronograma30Passos;

public class Cronograma30PassosFiltro extends FiltroListagem {

	private String nome;
	private Cronograma30Passos cronogramaPai;

	public String getNome() {
		return nome;
	}
	public Cronograma30Passos getCronogramaPai() {
		return cronogramaPai;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCronogramaPai(Cronograma30Passos cronogramaPai) {
		this.cronogramaPai = cronogramaPai;
	}
}
