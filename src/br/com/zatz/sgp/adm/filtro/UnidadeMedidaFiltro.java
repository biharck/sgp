package br.com.zatz.sgp.adm.filtro;


import org.nextframework.controller.crud.FiltroListagem;


public class UnidadeMedidaFiltro extends FiltroListagem {

	private String nome;
	
	public String getNome() {
        return nome;
    }
	

    public void setNome(String nome) {
        this.nome = nome;
    }
  
}
