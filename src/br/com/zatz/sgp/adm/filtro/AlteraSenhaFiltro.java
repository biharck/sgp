package br.com.zatz.sgp.adm.filtro;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.Required;

public class AlteraSenhaFiltro extends FiltroListagem {

	private String senhaAtual;
	private String senhaNova;
	private String reSenhaNova;

	@DisplayName("Senha Atual")
	@Required
	@Password
	public String getSenhaAtual() {
		return senhaAtual;
	}
	@DisplayName("Nova Senha")
	@Required
	@Password
	public String getSenhaNova() {
		return senhaNova;
	}
	@DisplayName("Confirmação Senha")
	@Required
	@Password
	public String getReSenhaNova() {
		return reSenhaNova;
	}
	
	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}
	public void setSenhaNova(String senhaNova) {
		this.senhaNova = senhaNova;
	}
	public void setReSenhaNova(String reSenhaNova) {
		this.reSenhaNova = reSenhaNova;
	}
	
}
