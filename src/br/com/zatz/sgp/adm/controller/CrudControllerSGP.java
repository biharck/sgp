package br.com.zatz.sgp.adm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nextframework.controller.NoActionHandlerException;
import org.nextframework.controller.crud.CrudController;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.util.SGPException;

public class CrudControllerSGP<FILTRO extends FiltroListagem, FORMBEAN, BEAN> extends CrudController<FiltroListagem, FORMBEAN, BEAN>{

	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,FORMBEAN form) {
		return new ModelAndView("crud/"+getBeanName()+"Entrada");	
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, BEAN bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch (Exception e) {
			e.printStackTrace();
			throw new SGPException("Ups! Houve uma falha ao tentar salvar este registro! Entre em contato com o administrador do sistema ou tente novamente por favor!");
		}
		request.addMessage("Registro salvo com sucesso!");
	}
	
	@Override
	protected void excluir(WebRequestContext request, BEAN bean)throws Exception {
		try{
			super.excluir(request, bean);
		}catch (Exception e) {
			throw new SGPException("Ups! Houve uma falha ao tentar excluir este registro! Entre em contato com o administrador do sistema ou tente novamente por favor!");
		}
		request.addMessage("Registro exclu�do com sucesso!");
	}
	
	@Override
	protected ModelAndView noActionHandler(HttpServletRequest request, HttpServletResponse response, NoActionHandlerException e)throws NoActionHandlerException {
		return sendRedirectToAction(null);
	}
	
}
