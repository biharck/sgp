package br.com.zatz.sgp.adm.controller;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/adm",authorizationModule=ProcessAuthorizationModule.class)
public class IndexController extends MultiActionController {
	
	@DefaultAction
	public ModelAndView action(WebRequestContext request){
		return new ModelAndView("index");
	}
}
