package br.com.zatz.sgp.adm.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.ComponenteFiltro;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Cronograma30PassosComponente;
import br.com.zatz.sgp.bean.Desvio;
import br.com.zatz.sgp.bean.PosicaoDesenho;
import br.com.zatz.sgp.bean.Projeto;
import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.dao.ArquivoDAO;
import br.com.zatz.sgp.service.AnexosComponenteService;
import br.com.zatz.sgp.service.ComponenteService;
import br.com.zatz.sgp.service.Cronograma30PassosComponenteService;
import br.com.zatz.sgp.service.DesvioService;
import br.com.zatz.sgp.service.ProjetoService;
import br.com.zatz.sgp.service.SchedaService;

@Controller(path="/adm/crud/Componente",authorizationModule=CrudAuthorizationModule.class)
public class ComponenteCrud extends CrudControllerSGP<ComponenteFiltro, Componente, Componente>{

	private ArquivoDAO arquivoDAO;
	private ProjetoService projetoService;
	private SchedaService schedaService;
	private ComponenteService componenteService;
	private AnexosComponenteService anexosComponenteService;
	private Cronograma30PassosComponenteService cronograma30PassosComponenteService;
	private DesvioService desvioService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setSchedaService(SchedaService schedaService) {
		this.schedaService = schedaService;
	}
	public void setComponenteService(ComponenteService componenteService) {
		this.componenteService = componenteService;
	}
	public void setAnexosComponenteService(AnexosComponenteService anexosComponenteService) {
		this.anexosComponenteService = anexosComponenteService;
	}
	public void setCronograma30PassosComponenteService(Cronograma30PassosComponenteService cronograma30PassosComponenteService) {
		this.cronograma30PassosComponenteService = cronograma30PassosComponenteService;
	}
	public void setDesvioService(DesvioService desvioService) {
		this.desvioService = desvioService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Componente form)throws Exception {
		if(request.getParameter("janelaEntrada")!=null)
			request.setAttribute("janelaEntrada", request.getParameter("janelaEntrada"));
		
		if(form!=null && form.getId()!=null){
			try {
				if(form.getFoto()!=null && form.getFoto().getCdfile()!=null){
					arquivoDAO.loadAsImage(form.getFoto());
					DownloadFileServlet.addCdfile(request.getSession(), form.getFoto().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			List<Scheda> schedasByComponente = schedaService.getSchedasByComponente(form.getId().toString());
			request.setAttribute("schedasByComponente", schedasByComponente);

			List<Desvio> desviosByComponente = desvioService.getDesviosByComponente(form.getId().toString());
			request.setAttribute("desviosByComponente", desviosByComponente);
			
			request.setAttribute("schedasAbertas", schedaService.getSchedasNaoFechadasByComponente(form.getId().toString()).size());
			
			form.setAnexos(anexosComponenteService.getAnexosComponentesByComponente(form));
			request.setAttribute("complementoURL", " Projeto "+form.getProjeto().getNome()+" <img src='/SGP/img/style/next.png'> ");	
			request.setAttribute("complemento2URL", ": " + form.getNome());	
			
			/*Tratamento para o cronograma 30 passos*/
			List<Cronograma30PassosComponente> cronogramas = cronograma30PassosComponenteService.getCronogramas(form);
			form.setListaCronograma30PassosComponente(cronogramas);
			request.setAttribute("cronogramas", cronogramas);
			
		}else{
			request.getSession().setAttribute("showpicture", false);
		}
		super.entrada(request, form);
	}
	
	@Action("copiar")
	public ModelAndView copiar(WebRequestContext request, Componente form)throws Exception {
		if(request.getParameter("janelaEntrada")!=null)
			request.setAttribute("janelaEntrada", request.getParameter("janelaEntrada"));
		form = componenteService.loadForEntrada(form);
		
		request.getSession().setAttribute("showpicture", false);
		request.setAttribute("schedasAbertas", 0);
		request.setAttribute("ACAO", "criar");
		request.setAttribute("complementoURL", " Projeto "+form.getProjeto().getNome()+" <img src='/SGP/img/style/next.png'> ");	
		
		Componente componenteClonado = new Componente();
		componenteClonado.setNome(form.getNome());
		componenteClonado.setNumero(form.getNumero());
		componenteClonado.setDescricao(form.getDescricao());
		componenteClonado.setMatricula(form.getMatricula());
		componenteClonado.setMassa(form.getMassa());
		componenteClonado.setNumeroTCAE(form.getNumeroTCAE());
		componenteClonado.setProjeto(form.getProjeto());
		componenteClonado.setProjetosAplicados(form.getProjetosAplicados());
		componenteClonado.setRevisao(form.getRevisao());
		componenteClonado.setVdVt(form);
		componenteClonado.setPosicoesDesenhos(form.getPosicoesDesenhos());
		componenteClonado.setAnexos(form.getAnexos());
		if(form.getPosicoesDesenhos()==null)
			for (PosicaoDesenho pd : componenteClonado.getPosicoesDesenhos()) {
				pd.setId(null);
			}
		componenteClonado.setAnexos(null);
		componenteClonado.setNumero("");
		componenteClonado.setNumeroTCAE("");
		componenteClonado.setCopia(true);
		return super.doEntrada(request, componenteClonado);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		if(request.getParameter("idProjeto")!=null){
			Projeto p = new Projeto(Integer.parseInt(request.getParameter("idProjeto")));
			p = projetoService.loadForEntrada(p);
			ComponenteFiltro _filtro = (ComponenteFiltro) filtro;
			_filtro.setProjeto(p);
			request.setAttribute("complementoURL", " Projeto "+p.getNome()+" <img src='/SGP/img/style/next.png'> ");
			super.doListagem(request, _filtro);
		}
		return super.doListagem(request, filtro);
	}
}
