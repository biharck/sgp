package br.com.zatz.sgp.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.bean.Uf;
import br.com.zatz.sgp.util.DatabaseError;
import br.com.zatz.sgp.util.SGPException;

@Controller(path="/adm/crud/Uf",authorizationModule=CrudAuthorizationModule.class)
public class UfCrud extends CrudControllerSGP<FiltroListagem,Uf,Uf> {
	
	@Override
	protected void salvar(WebRequestContext request, Uf bean) throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException  e) {
			if (DatabaseError.isKeyPresent(e, "unique_uf")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe um registro de Unidade Federativa com esta sigla cadastrada no sistema.");
			}
		}
	}

}
