package br.com.zatz.sgp.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.ClienteFiltro;
import br.com.zatz.sgp.bean.Cliente;
import br.com.zatz.sgp.dao.ArquivoDAO;

@Controller(path="/adm/crud/Cliente",authorizationModule=CrudAuthorizationModule.class)
public class ClienteCrud extends CrudControllerSGP<ClienteFiltro, Cliente, Cliente>{

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cliente form)	throws Exception {
		if(form!=null && form.getId()!=null){
			try {
				form.setUf(form.getMunicipio().getUf());
				if(form.getLogomarca()!=null && form.getLogomarca().getCdfile()!=null){
					arquivoDAO.loadAsImage(form.getLogomarca());
					DownloadFileServlet.addCdfile(request.getSession(), form.getLogomarca().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			request.getSession().setAttribute("showpicture", false);
		}
		super.entrada(request, form);
	}
}
