package br.com.zatz.sgp.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.persistence.ResultList;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.UsuarioFiltro;
import br.com.zatz.sgp.bean.EnumAtivo;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.service.PapelService;
import br.com.zatz.sgp.util.DatabaseError;
import br.com.zatz.sgp.util.SGPException;

@Controller(path = "/adm/crud/Usuario", authorizationModule = CrudAuthorizationModule.class)
public class UsuarioCrud extends CrudControllerSGP<UsuarioFiltro, Usuario, Usuario> {

	private PapelService papelService;

	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,
			FiltroListagem filtro) throws CrudException {
		UsuarioFiltro _filtro = (UsuarioFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		return super.doListagem(request, filtro);
	}
	
	
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)throws Exception {
		request.setAttribute("listaPapel", papelService.findAll());
	}

	@Override
	protected void entrada(WebRequestContext request, Usuario form) throws Exception {
		request.setAttribute("listaPapel", papelService.findAll());
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Usuario bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_login")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe um registro de Usu�rio com este Login cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
			}
		}
	}

}
