package br.com.zatz.sgp.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.UnidadeMedidaFiltro;
import br.com.zatz.sgp.bean.UnidadeMedida;
import br.com.zatz.sgp.util.DatabaseError;
import br.com.zatz.sgp.util.SGPException;

@Controller(path="/adm/crud/UnidadeMedida",authorizationModule=CrudAuthorizationModule.class)
public class UnidadeMedidaCrud extends CrudControllerSGP<UnidadeMedidaFiltro,UnidadeMedida,UnidadeMedida> {

	@Override
	protected void salvar(WebRequestContext request, UnidadeMedida bean)
			throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe um registro de Unidade de Medida com este Nome cadastrado no sistema.");
			}
		}
	}
}
