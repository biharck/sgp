package br.com.zatz.sgp.adm.controller.crud;

import java.util.Calendar;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.bean.SchedaComponente;
import br.com.zatz.sgp.service.ClienteService;
import br.com.zatz.sgp.service.SchedaService;
import br.com.zatz.sgp.util.SGPException;
import br.com.zatz.sgp.util.SGPUtil;

@Controller(path="/adm/process/controleScheda",authorizationModule=ProcessAuthorizationModule.class)
public class ControleSchedaProcess extends MultiActionController {

	private SchedaService schedaService;
	private ClienteService clienteService;
	
	public void setSchedaService(SchedaService schedaService) {
		this.schedaService = schedaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext context){
		return new ModelAndView("process/controleScheda");
	}
	
	public ModelAndView schedasParaVencer(WebRequestContext context){
		List<Scheda>  schedas = schedaService.getSchedasFaltandoMenos5Dias();
		if(schedas!=null && schedas.size() > 0){
			schedas = schedaService.getSchedasByIds(CollectionsUtil.listAndConcatenate(schedas, "id", ","));
			context.addMessage("Schedas Pendentes");
		}else
			context.addError("N�o Existem Schedas Pendentes");
		return new ModelAndView("process/controleScheda","schedas",schedas);
	}
	
	public ModelAndView schedasVencidas(WebRequestContext context){
		List<Scheda>  schedas = schedaService.getSchedasVencidas();
		if(schedas!=null && schedas.size() > 0){
			schedas = schedaService.getSchedasByIds(CollectionsUtil.listAndConcatenate(schedas, "id", ","));
			context.addMessage("Schedas Vencidas");
		}else
			context.addError("N�o Existem Schedas Vencidas");
		return new ModelAndView("process/controleScheda","schedas",schedas);
	}
	
	public ModelAndView sendMailSchedasParaVencer(WebRequestContext context){
		List<Scheda>  schedas = schedaService.getSchedasFaltandoMenos5Dias();
		schedas = schedaService.getSchedasByIds(CollectionsUtil.listAndConcatenate(schedas, "id", ","));
		for (Scheda scheda : schedas) {
			enviaEmailSchedas(scheda,true);
		}
		context.addMessage("E-mail enviado com sucesso!");
		return new ModelAndView("process/controleScheda","schedas",schedas);
	}
	
	public ModelAndView sendMailSchedasVencidas(WebRequestContext context){
		List<Scheda>  schedas = schedaService.getSchedasVencidas();
		schedas = schedaService.getSchedasByIds(CollectionsUtil.listAndConcatenate(schedas, "id", ","));
		for (Scheda scheda : schedas) {
			enviaEmailSchedas(scheda,false);
		}
		context.addMessage("E-mail enviado com sucesso!");
		return new ModelAndView("process/controleScheda","schedas",schedas);
	}
	
	
	
	
	/**
	 * <p>M�todo para enviar email de scheda pendentes e em aberto
	 *
	 * @param scheda {@link Scheda}
	 * @param aberto {@link Boolean} true para aberto e false para vencidas
	 */
	public void enviaEmailSchedas(Scheda scheda, boolean aberto){
		try {
			String dataVencimento = SGPUtil.dateToString(SGPUtil.incrementDate(scheda.getDataSolicitacao(), 15, Calendar.DAY_OF_MONTH));
			Scheda s = new Scheda();
			s.setId(scheda.getId());
			s = schedaService.loadForEntrada(scheda);
			String nomeProjeto = s.getSchedaComponente().get(0).getComponente().getProjeto().getNome();
			String numeroProjeto = s.getNumero();
			String cliente = clienteService.getClienteByScheda(s).getNomeFantasia();
			/**
			 * Otimizar o tempo de envio de e-mail
			 */
			for (SchedaComponente sc : s.getSchedaComponente()){
				if(aberto){
					SGPUtil.sendMailSchedasEmAberto("Scheda Pendente n�: #"+ scheda.getNumero() +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							nomeProjeto, 
							numeroProjeto, 
							dataVencimento, 
							cliente, 
							sc.getComponente().getProjectChief().getEmail(), 
							sc.getComponente().getProjectChief().getNome()
							);
				}else{
					SGPUtil.sendMailSchedasVencidas("Scheda Vencida n�: #"+ scheda.getNumero() +" para o componente #"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							"#"+sc.getComponente().getNumero() + " - " +sc.getComponente().getNome(), 
							nomeProjeto, 
							numeroProjeto, 
							dataVencimento, 
							cliente, 
							sc.getComponente().getProjectChief().getEmail(), 
							sc.getComponente().getProjectChief().getNome()
							);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SGPException("Ups! uma falha ocorreu ao tentar enviar um e-mail sobre uma nova Scheda!" +
					" Tente novamente mais tarde ou entre em contato com os administradores do Sistema. Obrigado! :) ");
		} 
		
	}
	
	
}
