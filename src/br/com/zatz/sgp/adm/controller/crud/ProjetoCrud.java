package br.com.zatz.sgp.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.ProjetoFiltro;
import br.com.zatz.sgp.bean.Projeto;

@Controller(path="/adm/crud/Projeto",authorizationModule=CrudAuthorizationModule.class)
public class ProjetoCrud extends CrudControllerSGP<ProjetoFiltro, Projeto, Projeto>{

}
