package br.com.zatz.sgp.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.bean.Municipio;

@Controller(path="/adm/crud/Municipio",authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudControllerSGP<FiltroListagem,Municipio,Municipio> {
	

}
