package br.com.zatz.sgp.adm.controller.crud;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.PapelFiltro;
import br.com.zatz.sgp.bean.Papel;

@Controller(path = "/adm/crud/Papel", authorizationModule = CrudAuthorizationModule.class)
public class PapelCrud extends CrudControllerSGP<PapelFiltro, Papel, Papel> {

	public void exemploAjax(WebRequestContext request) throws IOException,JSONException {
		
		/*parametros*/
		System.out.println();
		System.out.println("PARAMETRO ENVIADOS PELA REQUISI��O");
		System.out.println(request.getParameter("id"));
		System.out.println(request.getParameter("nome"));
		System.out.println();
		
		
		JSONObject jsonObj = new JSONObject();
		/*preparando retorno do ajax*/
		if (true) {//caso esteja ok
			jsonObj.put("erro", false);
			jsonObj.put("id", 10);
			jsonObj.put("nome", "um nome qualquer");
			jsonObj.put("endereco", "um endereco qualquer");
		} else {//caso queira lan�ar exception
			jsonObj.put("erro", true);
			jsonObj.put("msg", "uma mensagem de erro");
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}

}
