package br.com.zatz.sgp.adm.controller.crud;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.SchedaFiltro;
import br.com.zatz.sgp.bean.Interacoes;
import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.bean.SchedaComponente;
import br.com.zatz.sgp.bean.Status;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.service.ClienteService;
import br.com.zatz.sgp.service.InteracoesService;
import br.com.zatz.sgp.service.SchedaService;
import br.com.zatz.sgp.service.StatusService;
import br.com.zatz.sgp.util.DatabaseError;
import br.com.zatz.sgp.util.SGPException;

@Controller(path = "/adm/crud/Scheda", authorizationModule = CrudAuthorizationModule.class)
public class SchedaCrud extends CrudControllerSGP<SchedaFiltro,Scheda, Scheda> {
	
	private StatusService statusService;
	private SchedaService schedaService;
	private ClienteService clienteService;
	private InteracoesService interacoesService;
	private TransactionTemplate transactionTemplate;
	
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	public void setSchedaService(SchedaService schedaService) {
		this.schedaService = schedaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setInteracoesService(InteracoesService interacoesService) {
		this.interacoesService = interacoesService;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Scheda form)throws Exception {
		if(form.getId()==null)
			form.setStatus(statusService.loadForEntrada(new Status(1)));
		else{
			StringBuilder componentes = new StringBuilder("");
			for (SchedaComponente sc : form.getSchedaComponente()) 
				componentes
					.append("<br />")
					.append("<a class='tootip-center' title='Project Chief do Componente: ").append(sc.getComponente().getProjectChief().getNome()).append("' >")
					.append(" # ")
					.append(sc.getComponente().getNumero())
					.append("  -  Nome: ")
					.append(sc.getComponente().getNome())
					.append("<a/>")
					;
					
			request.setAttribute("nomeComponentes", componentes.toString());
			request.setAttribute("nomeProjeto", form.getSchedaComponente().get(0).getComponente().getProjeto().getNome());
			request.setAttribute("cliente", clienteService.getClienteByScheda(form).getNomeFantasia());
			super.entrada(request, form);
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Scheda form)throws CrudException {
		if(request.getParameter("showMenu")!=null && request.getParameter("showMenu").equals("false")){
			try{
				schedaService.saveOrUpdate(form);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "unique_scheda")){ //nota o index na tabela tem q ter o mesmo nome
					throw new SGPException("J� existe uma Scheda com este n�mero cadastrada no sistema.");
				}
			}
			return redirectClosePage(request);
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Scheda bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_scheda")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe uma Scheda com este n�mero cadastrada no sistema.");
			}
		}
	}
	
	public ModelAndView redirectClosePage(WebRequestContext request){
		return new ModelAndView("crud/schedaSaveSuccessProcess");
	}
	
	public ModelAndView reabrir(WebRequestContext request, final Scheda form){
		final Integer id = Integer.parseInt(request.getParameter("id"));
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				Scheda s = new Scheda();
				s.setId(id);
				s = schedaService.loadForEntrada(s);
				String nomeProjeto = s.getSchedaComponente().get(0).getComponente().getProjeto().getNome();
				String numeroProjeto = s.getNumero();
				Interacoes interacoes = new Interacoes();
				Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:MM:s");
				interacoes.setDetalhes("Scheda Reaberta por "+((Usuario)NextWeb.getUser()).getNome() +" no dia "+format.format(dataAtual));
				interacoes.setScheda(s);
				interacoesService.saveOrUpdate(interacoes);
				try {
					schedaService.enviaEmailReaberturaScheda(s.getNumero(),s.getSchedaComponente(), nomeProjeto, numeroProjeto);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				schedaService.updateStatusScheda(id,4);
				return null;
			}
		});
		return redirectClosePage(request);
	}
}
