package br.com.zatz.sgp.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.Cronograma30PassosFiltro;
import br.com.zatz.sgp.bean.Cronograma30Passos;

@Controller(path="/adm/crud/Cronograma30Passos", authorizationModule=CrudAuthorizationModule.class)
public class Cronograma30PassosCrud extends	CrudControllerSGP<Cronograma30PassosFiltro, Cronograma30Passos, Cronograma30Passos> {

}
