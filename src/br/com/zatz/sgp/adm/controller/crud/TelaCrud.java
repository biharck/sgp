package br.com.zatz.sgp.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.bean.Tela;

@Controller(path = "/adm/crud/Tela", authorizationModule = CrudAuthorizationModule.class)
public class TelaCrud extends CrudControllerSGP<FiltroListagem, Tela, Tela> {


}
