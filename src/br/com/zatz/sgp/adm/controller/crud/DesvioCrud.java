package br.com.zatz.sgp.adm.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.adm.controller.CrudControllerSGP;
import br.com.zatz.sgp.adm.filtro.DesvioFiltro;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Desvio;
import br.com.zatz.sgp.bean.DesvioComponente;
import br.com.zatz.sgp.service.DesvioService;
import br.com.zatz.sgp.util.DatabaseError;
import br.com.zatz.sgp.util.SGPException;

@Controller(path="/adm/crud/Desvio", authorizationModule=CrudAuthorizationModule.class)
public class DesvioCrud extends CrudControllerSGP<DesvioFiltro, Desvio, Desvio> {

	private DesvioService desvioService;
	public void setDesvioService(DesvioService desvioService) {
		this.desvioService = desvioService;
	}
	
	
	@Override
	protected void entrada(WebRequestContext request, Desvio form)throws Exception {
		
		if(form!=null && form.getId()!=null){
			List<Componente> componentes = new ArrayList<Componente>();
			if(form.getDesvioComponente()!=null){
				for (DesvioComponente desvios : form.getDesvioComponente()) {
					componentes.add(desvios.getComponente());
				}
				form.setProjeto(componentes.get(0).getProjeto());
				form.setComponente(componentes.get(0));
				form.setDesenhos(componentes);
			}
		}
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Desvio form)throws CrudException {
		if(request.getParameter("showMenu")!=null && request.getParameter("showMenu").equals("false")){
			try{
				desvioService.saveOrUpdate(form);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "unique_desvio")){ //nota o index na tabela tem q ter o mesmo nome
					throw new SGPException("J� existe um Desvio com este n�mero cadastrado no sistema.");
				}
			}
			return redirectClosePage(request);
		}
		return super.doSalvar(request, form);
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Desvio bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_desvio")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SGPException("J� existe um Desvio com este n�mero cadastrado no sistema.");
			}
		}
	}
	
	public ModelAndView redirectClosePage(WebRequestContext request){
		return new ModelAndView("crud/desvioSaveSuccessProcess");
	}
}
