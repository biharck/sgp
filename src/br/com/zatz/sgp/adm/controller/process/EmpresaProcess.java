package br.com.zatz.sgp.adm.controller.process;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.bean.Empresa;
import br.com.zatz.sgp.dao.ArquivoDAO;
import br.com.zatz.sgp.dao.EmpresaDAO;

@Controller(path="/adm/process/Empresa", authorizationModule=ProcessAuthorizationModule.class)
public class EmpresaProcess extends MultiActionController{
	
	private EmpresaDAO empresaDAO;
	private ArquivoDAO arquivoDAO;
	
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request,Empresa empresa){
		empresa = empresaDAO.getEmpresa();

		if(empresa!=null && empresa.getId()!=null){
			try {
				empresa.setUf(empresa.getMunicipio().getUf());
				if(empresa.getLogomarca()!=null && empresa.getLogomarca().getCdfile()!=null){
					arquivoDAO.loadAsImage(empresa.getLogomarca());
					DownloadFileServlet.addCdfile(request.getSession(), empresa.getLogomarca().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			empresa = new Empresa();
			request.getSession().setAttribute("showpicture", false);
		}
		return new ModelAndView("process/empresa","empresa",empresa);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, Empresa bean){
		
		try {
			if(bean.getLogomarca()!=null && bean.getLogomarca().getCdfile()!=null)
				bean.setLogomarca(arquivoDAO.load(bean.getLogomarca()));
			if(bean.getLogomarca()!=null)
				arquivoDAO.saveOrUpdate(bean.getLogomarca());
			empresaDAO.saveOrUpdate(bean);
			if(bean.getLogomarca()!=null && bean.getLogomarca().getCdfile()!=null){
				arquivoDAO.loadAsImage(bean.getLogomarca());
				DownloadFileServlet.addCdfile(request.getSession(), bean.getLogomarca().getCdfile());
				request.getSession().setAttribute("showpicture", true);
			}else
				request.getSession().setAttribute("showpicture", false);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Falha ao salvar o registro, entre em contato com o administrador do sistema");
		}
		request.addMessage("Registro salvo com sucesso");
		return index(request, bean);
	}

}
