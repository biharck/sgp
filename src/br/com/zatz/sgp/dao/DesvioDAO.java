package br.com.zatz.sgp.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.zatz.sgp.adm.filtro.DesvioFiltro;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Desvio;

public class DesvioDAO extends GenericDAOSGP<Desvio>{

	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("desvioComponente",true);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Desvio> query) {
		query.leftOuterJoinFetch("desvio.desvioComponente desvioComponente");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Desvio> query,FiltroListagem _filtro) {
		DesvioFiltro filtro = (DesvioFiltro) _filtro;
		query
			.where("desvio.numero=?",filtro.getNumero())
			.leftOuterJoin("desvio.desvioComponente dc")
			.leftOuterJoin("dc.componente c")
			.leftOuterJoin("c.projeto p")
			.where("c=?",filtro.getComponente())
			.where("p=?",filtro.getProjeto())
			.whereLikeIgnoreAll("desvio.denominacao", filtro.getDenominacao());
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Desvios de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @return {@link List} {@link Desvio}
	 */
	public List<Desvio> getDesviosByComponente(String idComponentes){
		return query()
				.select("desvio")
				.leftOuterJoin("desvio.desvioComponente dc")
				.leftOuterJoin("dc.componente c")
				.whereIn("c.id", idComponentes)
				.list();
	}
}
