package br.com.zatz.sgp.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.zatz.sgp.adm.filtro.ComponenteFiltro;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Projeto;

public class ComponenteDAO extends GenericDAOSGP<Componente>{

	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("posicoesDesenhos",true);
		save.saveOrUpdateManaged("anexos",true);
		save.saveOrUpdateManaged("listaCronograma30PassosComponente",true);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Componente> query,	FiltroListagem _filtro) {

		ComponenteFiltro filtro = (ComponenteFiltro) _filtro;
		
		query
		.whereLikeIgnoreAll("componente.nome", filtro.getNome())
		.where("componente.projeto=?",filtro.getProjeto())
		.whereLikeIgnoreAll("componente.numero",filtro.getNumero())
		.whereLikeIgnoreAll("componente.matricula",filtro.getMatricula())
		.whereLikeIgnoreAll("componente.numeroTCAE",filtro.getNumeroTCAE())
		.leftOuterJoin("componente.projectChief projectChief")
		.where("projectChief=?",filtro.getProjectChief())
		;
	}
	
	public List<Componente> findComponentesByProjeto(Projeto projeto){
		return query()
				.select("componente.nome, componente.id")
				.leftOuterJoin("componente.projeto p")
				.groupBy("componente.nome")
				.where("p=?",projeto)
				.list();
	}

	public List<Componente> findDesenhosByComponentes(Componente c, Projeto p){
		return query()
				.select("componente.id, componente.numero")
				.leftOuterJoin("componente.projeto p")
				.where("p=?",p)
				.where("componente.matricula=?",load(c).getMatricula())
				.list();
	}
	
	public Componente getComponenteById(Integer id){
		return query()
				.select("componente")			
				.where("componente.id=?",id)
				.unique();
	}
}
