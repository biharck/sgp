package br.com.zatz.sgp.dao;


import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.util.CollectionsUtil;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.zatz.sgp.adm.filtro.UsuarioFiltro;
import br.com.zatz.sgp.bean.EnumAtivo;
import br.com.zatz.sgp.bean.Papel;
import br.com.zatz.sgp.bean.PapelUsuario;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.service.UsuarioService;
import br.com.zatz.sgp.util.SGPException;


@DefaultOrderBy("usuario.nome")
public class UsuarioDAO extends GenericDAOSGP<Usuario> {
	
	PapelDAO papelDAO;
	PapelUsuarioDAO papelUsuarioDAO;
	
	public void setPapelUsuarioDAO(PapelUsuarioDAO papelUsuarioDAO) {
		this.papelUsuarioDAO = papelUsuarioDAO;
	}
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public Usuario findByLogin(String login) {
		return query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())			
			.unique();
	}
	
	
	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	
	public PapelDAO getPapelDAO() {
		return papelDAO;
	}
	
	
	@Override
	public void saveOrUpdate(final Usuario bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				UsuarioService usuarioService = new UsuarioService();
				Usuario load;
				
				//valida se login existe
				Usuario usuarioSalvo = findByLogin(bean.getLogin());
				if(usuarioSalvo!=null){
					boolean ehEdicao = bean.getId()!=null;
					boolean ehMesmoObjeto = usuarioSalvo.getId().equals(bean.getId());
					if (!ehEdicao || ehEdicao && !ehMesmoObjeto){
						throw new SGPException("O login j� existe, digite outro login");
					}
				} 
					
				
				/* Insert */
				if(bean.getId() == null){
					load = new Usuario();
					if(!bean.getSenha().equals(bean.getReSenha())){
						throw new SGPException("A senha e a confirma��o devem ser iguais.");
					}
					
//					if(!bean.getEmail().equals(bean.getReEmail())){
//						throw new SGPException("O email e a confirma��o devem ser iguais.");
//					}
					usuarioService.criptografaSenha(bean);
					load.setSenha(bean.getSenha());
				}else
					load = load(bean);
				
				load.setNome(bean.getNome());
				load.setLogin(bean.getLogin());
				load.setEmail(bean.getEmail());
				load.setAtivo(bean.isAtivo());
				load.setSenhaProvisoria(bean.isSenhaProvisoria());
							
				UsuarioDAO.super.saveOrUpdate(load);
				
				
				papelDAO.deleteByUsuario(load);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						PapelUsuario papelUsuario = new PapelUsuario();
						papelUsuario.setPapel(papel);
						papelUsuario.setUsuario(load);
						papelUsuarioDAO.saveOrUpdate(papelUsuario);
					}
				}
				return null;
			}
			
		});
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Usuario> query,FiltroListagem _filtro) {
		UsuarioFiltro filtro = (UsuarioFiltro) _filtro;

		query.select("usuario.id, usuario.nome, usuario.login,usuario.ativo,usuario.dtUltimoLogin, "+
				"papel.nome, papel.id")
		.join("usuario.papeisUsuario pu")
		.join("pu.papel papel")
		.whereLikeIgnoreAll("usuario.nome", filtro.getNome());
		
		if(filtro.getPapeis()!=null){
			String idsPapel = CollectionsUtil.listAndConcatenate(filtro.getPapeis(), "id", ",");
			query.whereIn("pu.papel", idsPapel);
		}
		
		if(filtro.getAtivo()!=null)			
			query.where("usuario.ativo is "+filtro.getAtivo().isOptBool());
		
	}

	
	public void alteraSenha(Usuario bean) {
		getJdbcTemplate().update("update usuario set senha = ?, senhaAnterior = ?, senhaAnterior2 = ?, senhaProvisoria = ?" +
				" where id = ?",bean.getSenha(),bean.getSenhaAnterior(), bean.getSenhaAnterior2(), bean.isSenhaProvisoria(), bean.getId());
	}
	
	public void alteraDtUltimoLogin(Usuario bean) {
		getJdbcTemplate().update("update usuario set dtUltimoLogin = ?" +
				" where id = ?", bean.getDtUltimoLogin(), bean.getId());
	}
	
}
