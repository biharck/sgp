package br.com.zatz.sgp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.zatz.sgp.adm.filtro.SchedaFiltro;
import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Scheda;
import br.com.zatz.sgp.util.MysqlConnection;

public class SchedaDAO extends GenericDAOSGP<Scheda> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("interacoes",true);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Scheda> query) {
		query
		.select("scheda.id,scheda.userInc,scheda.timeInc, i.timeInc, scheda.timeAlt , scheda.userAlt, scheda.descricao,scheda.numero," +
				"scheda.investimento,scheda.projectChief,scheda.dataSolicitacao, i.id, i.detalhes," +
				"i.userInc,i.timeInc, a.id, arq.cdfile, arq.name, arq.contenttype, arq.size, arq.content, s.id, s.descricao," +
				"c.id, c.nome, c.numero,c.projectChief, p.id, p.nome, p.identificacao")
		.leftOuterJoin("scheda.status s")
		.leftOuterJoin("scheda.interacoes i")
		.leftOuterJoin("scheda.schedaComponente sc")
		.leftOuterJoin("sc.componente c")
		.leftOuterJoin("c.projeto p")
		.leftOuterJoin("i.anexos a")
		.leftOuterJoin("a.anexo arq")
		;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Scheda> query,FiltroListagem _filtro) {
		SchedaFiltro filtro = (SchedaFiltro) _filtro;

		query
		.whereLikeIgnoreAll("scheda.descricao", filtro.getDescricao())
		.leftOuterJoin("scheda.status s")
		.where("s=?",filtro.getStatus());
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Schedas de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @return {@link List} {@link Scheda}
	 */
	public List<Scheda> getSchedasByComponente(String idComponentes){
		return query()
				.select("scheda")
				.leftOuterJoin("scheda.schedaComponente sc")
				.leftOuterJoin("sc.componente c")
				.whereIn("c.id", idComponentes)
				.list();
	}
	
	/**
	 * <p>M�todo que devolve uma rela��o de Schedas n�o fechadas de um determinado componente</p>
	 * @param componente {@link Componente}
	 * @return {@link List} {@link Scheda}
	 */
	public List<Scheda> getSchedasNaoFechadasByComponente(String idComponentes){
		return query()
		.select("scheda")
		.leftOuterJoin("scheda.schedaComponente sc")
		.leftOuterJoin("sc.componente c")
		.leftOuterJoin("scheda.status status")
		.whereIn("c.id", idComponentes)
		.where("status.id <> 3")
		.list();
	}
	
	/**
	 * <p>M�todo respons�vel em atualizar o status de uma determinada Scheda</p>
	 * @param statusId {@link Integer}
	 */
	public void updateStatusScheda(Integer idScheda, Integer statusId){
		getJdbcTemplate().update("update scheda set status_id = "+statusId + " where id="+idScheda);
	}
	
	/**
	 * <p>M�todo que devolve todas as Schedas que est� para vencer, superior a 10 dias da
	 * data de solicita��o e inferior a data de vencimento</p>
	 * @return
	 */
	public List<Scheda> getSchedasFaltandoMenos5Dias(){
		
		List<Scheda> listScheda = new ArrayList<Scheda>();
		MysqlConnection m = new MysqlConnection();
		Connection con = m.getConnection();
		PreparedStatement pstmt = null;
        ResultSet rs = null;
		
		try {			
			pstmt = con.prepareStatement("select id from scheda where DATE_ADD(dataSolicitacao, INTERVAL 10 DAY) < CURDATE() and status_id <> 3" +
					" AND DATE_ADD(dataSolicitacao, INTERVAL 15 DAY) > CURDATE()");
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				Scheda Scheda = new Scheda();
				Scheda.setId(rs.getInt("id"));
				listScheda.add(Scheda);
			}
	        
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		} finally{
			try {
				con.close();
				System.out.println("Fechou a conex�o");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return listScheda;
	}
	
	/**
	 * <p>M�todo que devolve todas as Schedas que est� para vencer, superior a 10 dias da
	 * data de solicita��o e inferior a data de vencimento</p>
	 * @return
	 */
	public List<Scheda> getSchedasVencidas(){
		
		List<Scheda> listScheda = new ArrayList<Scheda>();
		MysqlConnection m = new MysqlConnection();
		Connection con = m.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {			
			pstmt = con.prepareStatement("select id from scheda where DATE_ADD(dataSolicitacao, INTERVAL 10 DAY) < CURDATE() and status_id <> 3" +
			" AND DATE_ADD(dataSolicitacao, INTERVAL 15 DAY) < CURDATE()");
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				Scheda Scheda = new Scheda();
				Scheda.setId(rs.getInt("id"));
				listScheda.add(Scheda);
			}
			
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		} finally{
			try {
				con.close();
				System.out.println("Fechou a conex�o");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return listScheda;
	}
	
	public List<Scheda> getSchedasByIds(String ids){
		return
			query()
			.select("scheda.id,scheda.userInc,scheda.timeInc, i.timeInc, scheda.timeAlt , scheda.userAlt, scheda.descricao,scheda.numero," +
					"scheda.investimento,scheda.projectChief,scheda.dataSolicitacao, i.id, i.detalhes," +
					"i.userInc,i.timeInc, a.id, arq.cdfile, arq.name, arq.contenttype, arq.size, arq.content, s.id, s.descricao," +
					"c.id, c.nome, c.numero,c.projectChief, p.id, p.nome, p.identificacao")
			.leftOuterJoin("scheda.status s")
			.leftOuterJoin("scheda.interacoes i")
			.leftOuterJoin("scheda.schedaComponente sc")
			.leftOuterJoin("sc.componente c")
			.leftOuterJoin("c.projeto p")
			.leftOuterJoin("i.anexos a")
			.leftOuterJoin("a.anexo arq")
			.whereIn("scheda.id", ids)
			.list()
			;
	}
	
}
