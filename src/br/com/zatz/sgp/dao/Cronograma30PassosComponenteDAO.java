package br.com.zatz.sgp.dao;

import java.util.List;

import br.com.zatz.sgp.bean.Componente;
import br.com.zatz.sgp.bean.Cronograma30PassosComponente;

public class Cronograma30PassosComponenteDAO extends GenericDAOSGP<Cronograma30PassosComponente>{

	
	public List<Cronograma30PassosComponente> getCronogramas(Componente componente){
		return query()
			.select("cronograma30PassosComponente.id, cronograma30PassosComponente.inicio, cronograma30PassosComponente.termino," +
					"c.id," +
					"c30p.id, c30p.nome,c30p.ordem," +
					"pai.id, pai.nome,pai.ordem")
			.leftOuterJoin("cronograma30PassosComponente.componente c")
			.leftOuterJoin("cronograma30PassosComponente.cronograma30Passos c30p")
			.leftOuterJoin("c30p.cronogramaPai pai")
			.where("c=?",componente)
			.list();
	}
}
