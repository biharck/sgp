package br.com.zatz.sgp.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.zatz.sgp.bean.PapelUsuario;


@DefaultOrderBy("usuario")
public class PapelUsuarioDAO extends GenericDAO<PapelUsuario> {

}
