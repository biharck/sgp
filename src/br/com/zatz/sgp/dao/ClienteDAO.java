package br.com.zatz.sgp.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.zatz.sgp.adm.filtro.ClienteFiltro;
import br.com.zatz.sgp.bean.Cliente;
import br.com.zatz.sgp.bean.Desvio;
import br.com.zatz.sgp.bean.Scheda;

@DefaultOrderBy("cliente.razaoSocial")
public class ClienteDAO extends GenericDAOSGP<Cliente>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<Cliente> query,FiltroListagem _filtro) {
		ClienteFiltro filtro = (ClienteFiltro) _filtro;
		query
		.where("cliente.cnpj=?",filtro.getCnpj())
		.whereLikeIgnoreAll("cliente.nomeFantasia", filtro.getNomeFantasia());
	}
	
	/**
	 * <p>Baseado em um determinado componente, este m�todo devolve o nome do clientes</p>
	 * 
	 * @param scheda {@link Scheda}
	 * @return {@link Cliente}
	 */
	public Cliente getClienteByScheda(Scheda scheda){
		return query()
				.select("cliente")
				.leftOuterJoin("cliente.projetosCliente pc")
				.leftOuterJoin("pc.componentes c")
				.leftOuterJoin("c.schedasComponentes sc")
				.leftOuterJoin("sc.scheda s")
				.where("s=?",scheda)
				.unique();
		
	}
	
	/**
	 * <p>Baseado em um determinado Desvio, este m�todo devolve o nome do clientes</p>
	 * 
	 * @param scheda {@link Scheda}
	 * @return {@link Cliente}
	 */
	public Cliente getClienteByDesvio(Desvio desvio){
		return query()
		.select("cliente")
		.leftOuterJoin("cliente.projetosCliente pc")
		.leftOuterJoin("pc.componentes c")
		.leftOuterJoin("c.desviosComponentes dc")
		.leftOuterJoin("dc.desvio d")
		.where("d=?",desvio)
		.unique();
		
	}
}
