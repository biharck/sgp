package br.com.zatz.sgp.dao;

import java.util.List;

import br.com.zatz.sgp.bean.AnexosComponente;
import br.com.zatz.sgp.bean.Componente;

public class AnexosComponenteDAO extends GenericDAOSGP<AnexosComponente> {

	
	public List<AnexosComponente> getAnexosComponentesByComponente(Componente componente){
		return query().select("anexosComponente").leftOuterJoin("anexosComponente.componente c").where("c=?",componente).list();
	}
}
