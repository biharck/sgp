package br.com.zatz.sgp.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.zatz.sgp.adm.filtro.ProjetoFiltro;
import br.com.zatz.sgp.bean.Projeto;

public class ProjetoDAO extends GenericDAOSGP<Projeto>{

	@Override
	public void updateListagemQuery(QueryBuilder<Projeto> query,FiltroListagem _filtro) {
		ProjetoFiltro filtro = (ProjetoFiltro) _filtro;
		
		query
		.whereLikeIgnoreAll("projeto.nome", filtro.getNome())
		.where("projeto.cliente=?",filtro.getCliente())
		.whereLikeIgnoreAll("projeto.identificacao",filtro.getIdentificacao())		
		;
	}
}
