package br.com.zatz.sgp.dao;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.zatz.sgp.adm.filtro.UnidadeMedidaFiltro;
import br.com.zatz.sgp.bean.UnidadeMedida;


@DefaultOrderBy("nome")
public class UnidadeMedidaDAO extends GenericDAOSGP<UnidadeMedida> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<UnidadeMedida> query,FiltroListagem _filtro) {
		UnidadeMedidaFiltro filtro = (UnidadeMedidaFiltro) _filtro;
		query.whereLikeIgnoreAll("unidadeMedida.nome", filtro.getNome());
	}

}
