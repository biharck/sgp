package br.com.zatz.sgp.dao;
import org.nextframework.persistence.DefaultOrderBy;

import br.com.zatz.sgp.bean.Municipio;
import br.com.zatz.sgp.bean.Uf;


@DefaultOrderBy("nome")
public class UfDAO extends GenericDAOSGP<Uf> {
	
	public Uf getUfByMunicipio(Municipio m){
		return query().join("uf.municipios m").where("m=?",m).unique();
	}
}
