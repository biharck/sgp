package br.com.zatz.sgp.dao;

import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.zatz.sgp.bean.Interacoes;

public class InteracoesDAO extends GenericDAOSGP<Interacoes>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("anexos",true);
	}

}
