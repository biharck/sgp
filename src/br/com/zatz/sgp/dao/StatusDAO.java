package br.com.zatz.sgp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import br.com.zatz.sgp.bean.Status;
import br.com.zatz.sgp.util.MysqlConnection;

public class StatusDAO extends GenericDAOSGP<Status>{
	
	/**
	 *  Retorna os status que um ticket pode obter
	 * @return List<Status>
	 */
	public List<Status> getStatusToSendEmail(){

		List<Status> listStatus = new ArrayList();
		MysqlConnection m = new MysqlConnection();
		Connection con = m.getConnection();
		PreparedStatement pstmt = null;
        ResultSet rs = null;
		
		try {
			pstmt = con.prepareStatement("SELECT * FROM status");
		
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				Status status = new Status();
				status.setId(rs.getInt("id"));
				status.setDescricao(rs.getString("descricao"));
				listStatus.add(status);
			}
	        
		} catch (Exception e) {
			e.getStackTrace();
		} finally{
			try {
				con.close();
				System.out.println("Fechou a conex�o");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return listStatus;
	}	
}
