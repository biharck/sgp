package br.com.zatz.sgp.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import br.com.zatz.sgp.adm.filtro.PapelFiltro;
import br.com.zatz.sgp.bean.Papel;
import br.com.zatz.sgp.bean.PapelUsuario;
import br.com.zatz.sgp.bean.Usuario;

@DefaultOrderBy("nome")
public class PapelDAO extends GenericDAO<Papel> {

	public List<Papel> findByUsuario(Usuario usuario) {
		if (usuario.getId() == null) {
			return new ArrayList<Papel>();
		}
		return query().select("papel").from(PapelUsuario.class)
				.leftOuterJoin("papelUsuario.papel papel")
				.where("papelUsuario.usuario = ?", usuario).list();
	}

	public void deleteByUsuario(Usuario usuario) {
		getHibernateTemplate().bulkUpdate(
				"delete from PapelUsuario where usuario = ?", usuario);
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Papel> query,
			FiltroListagem _filtro) {
		PapelFiltro filtro = (PapelFiltro) _filtro;

		if (filtro.getNome() != null)
			query.whereLikeIgnoreAll("papel.nome", filtro.getNome());
		else
			super.updateListagemQuery(query, _filtro);
	}
}
