package br.com.zatz.sgp.dao;

import java.awt.Image;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;

import org.nextframework.types.File;

import br.com.zatz.sgp.bean.Arquivo;
import br.com.zatz.sgp.util.SGPException;

public class ArquivoDAO extends GenericDAOSGP<Arquivo>{

	public Image loadAsImage(File file) {
		try {
			return ImageIO.read(new ByteArrayInputStream(file.getContent()));
		}
		catch (Exception e) {
			throw new SGPException("Erro ao converter arquivo em imagem.");
		}
	}
}
