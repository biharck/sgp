package br.com.zatz.sgp.dao;

import br.com.zatz.sgp.bean.Empresa;

public class EmpresaDAO extends GenericDAOSGP<Empresa>{
	
	/**
	 * <p>M�todo respons�vel em retornar os dados da cl�nica
	 * @return {@link Clinica}
	 * @author biharck
	 */
	public Empresa getEmpresa(){
		return
			query().select("empresa").unique();
	}
	
	

}
