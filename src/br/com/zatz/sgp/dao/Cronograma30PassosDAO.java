package br.com.zatz.sgp.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.zatz.sgp.adm.filtro.Cronograma30PassosFiltro;
import br.com.zatz.sgp.bean.Cronograma30Passos;

public class Cronograma30PassosDAO extends GenericDAOSGP<Cronograma30Passos> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Cronograma30Passos> query, FiltroListagem _filtro) {

		Cronograma30PassosFiltro filtro = (Cronograma30PassosFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("cronograma30Passos.nome",filtro.getNome())
			.where("cronograma30Passos.cronogramaPai=?",filtro.getCronogramaPai());
			
	}
	
	public List<Cronograma30Passos> getCronogramas(){
		return
		query()
		.select("cronograma30Passos.nome, cronograma30Passos.id, cronograma30Passos.ordem," +
				"cp.nome, cp.id, cp.ordem")
		.leftOuterJoin("cronograma30Passos.cronogramaPai cp")
		.list();
	}
	public List<Cronograma30Passos> getCronogramasPai(){
		return
		query()
		.select("pai.id")
		.join("cronograma30Passos.cronogramaPai pai")
		.groupBy("pai.id")
		.list();
	
	}
}
