package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="tela")
public class Tela extends BeanAuditoria{
	
	private String path;
	private String descricao;
	
	//GET
	@DescriptionProperty
	@Required
	@MaxLength(value=200)
	public String getPath() {
		return path;
	}
	@MaxLength(value=200)
	@Required
	public String getDescricao() {
		return descricao;
	}
	
	//SET
	public void setPath(String path) {
		this.path = path;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
