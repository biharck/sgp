package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="schedacomponente")
public class SchedaComponente extends BeanAuditoria {

	private Scheda scheda;
	private Componente componente;
	
	
	public SchedaComponente(){}
	
	public SchedaComponente(Scheda scheda, Componente componente){
		this.scheda = scheda;
		this.componente = componente;
	}
	
	@ManyToOne
	@ForeignKey(name="fk_schedacomponente_scheda")
	public Scheda getScheda() {
		return scheda;
	}
	@ManyToOne
	@ForeignKey(name="fk_schedacomponente_comp")
	public Componente getComponente() {
		return componente;
	}
	public void setScheda(Scheda scheda) {
		this.scheda = scheda;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
}
