package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="anexoscomponentes")
public class AnexosComponente extends BeanAuditoria {

	private Arquivo anexo;
	private Componente componente;
	
	@ManyToOne
	@ForeignKey(name="fk_anexoscomp_anexo")
	public Arquivo getAnexo() {
		return anexo;
	}
	@ManyToOne
	@ForeignKey(name="fk_anexoscomp_comp")
	public Componente getComponente() {
		return componente;
	}
	
	public void setAnexo(Arquivo anexo) {
		this.anexo = anexo;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
}
