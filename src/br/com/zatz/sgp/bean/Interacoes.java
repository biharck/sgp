package br.com.zatz.sgp.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="interacoes")
public class Interacoes extends BeanAuditoria {
	
	private String detalhes;
	private Scheda scheda;
	private List<AnexosInteracoes> anexos;
	private Money investimento;
	
	@Required
	@DescriptionProperty
	@Column(length=500)
	public String getDetalhes() {
		return detalhes;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_interacao_scheda")
	public Scheda getScheda() {
		return scheda;
	}
	
	@OneToMany(mappedBy="interacoes")
	public List<AnexosInteracoes> getAnexos() {
		return anexos;
	}
	@Transient
	public Money getInvestimento() {
		return investimento;
	}
	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}
	public void setScheda(Scheda scheda) {
		this.scheda = scheda;
	}
	public void setInvestimento(Money investimento) {
		this.investimento = investimento;
	}
	public void setAnexos(List<AnexosInteracoes> anexos) {
		this.anexos = anexos;
	}
}
