package br.com.zatz.sgp.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name="projeto")
public class Projeto extends BeanAuditoria{
	
	private String nome;
	private String identificacao;
	private String descricao;
	private Cliente cliente;
	private Usuario gerente;
	private Date preVP;
	private Date verificaProcesso;
	private Date preSerie;
	private Date sop;
	private List<Componente> componentes;
	private String nomeTransient;

	public Projeto(){}
	public Projeto(Integer id){this.id = id;}
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Transient
	public String getNomeTransient(){
		return getNome() + " - " + getIdentificacao();
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne
	public Cliente getCliente() {
		return cliente;
	}
	@ManyToOne
	public Usuario getGerente() {
		return gerente;
	}
	public Date getPreVP() {
		return preVP;
	}
	public Date getPreSerie() {
		return preSerie;
	}
	public Date getVerificaProcesso() {
		return verificaProcesso;
	}
	public Date getSop() {
		return sop;
	}
	@OneToMany(mappedBy="projeto")
	public List<Componente> getComponentes() {
		return componentes;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setGerente(Usuario gerente) {
		this.gerente = gerente;
	}
	public void setVerificaProcesso(Date verificaProcesso) {
		this.verificaProcesso = verificaProcesso;
	}
	public void setPreSerie(Date preSerie) {
		this.preSerie = preSerie;
	}
	public void setPreVP(Date preVP) {
		this.preVP = preVP;
	}
	public void setSop(Date sop) {
		this.sop = sop;
	}
	public void setComponentes(List<Componente> componentes) {
		this.componentes = componentes;
	}
	public void setNomeTransient(String nomeTransient) {
		this.nomeTransient = nomeTransient;
	}
}
