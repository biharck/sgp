package br.com.zatz.sgp.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="componente")
public class Componente extends BeanAuditoria implements Cloneable{

	private String nome;
	private String numero;
	private String descricao;
	private String matricula;
	private double massa;
	private String numeroTCAE;
	private Arquivo foto;
	private Projeto projeto;
	private List<Projeto> projetosAplicados;
	private List<SchedaComponente> schedasComponentes;
	private List<DesvioComponente> desviosComponentes;
	private Usuario projectChief;
	private Money investimento;
	private Money custoPeca;
	private List<PosicaoDesenho> posicoesDesenhos;
	private String revisao;
	private String nomeComponente;
	private Componente vdVt;
	private boolean copia;
	private List<AnexosComponente> anexos;
	private List<Cronograma30PassosComponente> listaCronograma30PassosComponente;
	
	private EnumAprovado benestare;
	private EnumAprovado autoQualificacao;
	private EnumAprovado provasInterrogativas;
	
	public Componente(){}
	public Componente(String nome, String numero, String descricao, String matricula, double massa, String numeroTCAE,
			Projeto projeto, List<Projeto> projetosAplicados, List<PosicaoDesenho> posicoesDesenhos, String revisao){
		this.nome = nome;
		this.numero = numero;
		this.descricao = descricao;
		this.matricula = matricula;
		this.massa = massa;
		this.numeroTCAE = numeroTCAE;
		this.projeto = projeto;
		this.projetosAplicados = projetosAplicados;
		this.posicoesDesenhos = posicoesDesenhos;
		this.revisao = revisao;
	}
	@Transient
	public boolean isCopia() {
		return copia;
	}
	@OneToMany(mappedBy="componente")	
	public List<AnexosComponente> getAnexos() {
		return anexos;
	}
	@OneToMany(mappedBy="componente")
	public List<Cronograma30PassosComponente> getListaCronograma30PassosComponente() {
		return listaCronograma30PassosComponente;
	}
	public EnumAprovado getBenestare() {
		return benestare;
	}
	public EnumAprovado getAutoQualificacao() {
		return autoQualificacao;
	}
	public EnumAprovado getProvasInterrogativas() {
		return provasInterrogativas;
	}
	
	public void setCopia(boolean copia) {
		this.copia = copia;
	}
	
	@DescriptionProperty
	@Transient
	public String getNomeComponente(){
		return (getNome()==null?"":getNome()) +" "+(getNumero()==null?"":getNumero());
	}
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public String getNumero() {
		return numero;
	}
	@MaxLength(255)
	public String getDescricao() {
		return descricao;
	}
	@Required
	public String getMatricula() {
		return matricula;
	}
	public double getMassa() {
		return massa;
	}
	@Required
	public String getNumeroTCAE() {
		return numeroTCAE;
	}
	@ManyToOne
	public Arquivo getFoto() {
		return foto;
	}
	@ManyToOne
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	public List<Projeto> getProjetosAplicados() {
		return projetosAplicados;
	}
	@OneToMany(mappedBy="componente")
	public List<SchedaComponente> getSchedasComponentes() {
		return schedasComponentes;
	}
	@OneToMany(mappedBy="componente")
	public List<DesvioComponente> getDesviosComponentes() {
		return desviosComponentes;
	}
	@Required
	@ManyToOne
	public Usuario getProjectChief() {
		return projectChief;
	}
	public Money getInvestimento() {
		return investimento;
	}
	public Money getCustoPeca() {
		return custoPeca;
	}
	@OneToMany(mappedBy="componente")
	public List<PosicaoDesenho> getPosicoesDesenhos() {
		return posicoesDesenhos;
	}
	@Required
	public String getRevisao() {
		return revisao;
	}
	@ManyToOne
	@ForeignKey(name="fk_componente_pai")
	public Componente getVdVt() {
		return vdVt;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setMassa(double massa) {
		this.massa = massa;
	}
	public void setNumeroTCAE(String numeroTCAE) {
		this.numeroTCAE = numeroTCAE;
	}
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setProjetosAplicados(List<Projeto> projetosAplicados) {
		this.projetosAplicados = projetosAplicados;
	}
	public void setSchedasComponentes(List<SchedaComponente> schedasComponentes) {
		this.schedasComponentes = schedasComponentes;
	}
	public void setDesviosComponentes(List<DesvioComponente> desviosComponentes) {
		this.desviosComponentes = desviosComponentes;
	}
	public void setProjectChief(Usuario projectChief) {
		this.projectChief = projectChief;
	}
	public void setInvestimento(Money investimento) {
		this.investimento = investimento;
	}
	public void setCustoPeca(Money custoPeca) {
		this.custoPeca = custoPeca;
	}
	public void setRevisao(String revisao) {
		this.revisao = revisao;
	}
	public void setPosicoesDesenhos(List<PosicaoDesenho> posicoesDesenhos) {
		this.posicoesDesenhos = posicoesDesenhos;
	}
	public void setNomeComponente(String nomeComponente) {
		this.nomeComponente = nomeComponente;
	}
	public void setVdVt(Componente vdVt) {
		this.vdVt = vdVt;
	}
	public void setAnexos(List<AnexosComponente> anexos) {
		this.anexos = anexos;
	}
	public void setListaCronograma30PassosComponente(List<Cronograma30PassosComponente> listaCronograma30PassosComponente) {
		this.listaCronograma30PassosComponente = listaCronograma30PassosComponente;
	}
	public void setAutoQualificacao(EnumAprovado autoQualificacao) {
		this.autoQualificacao = autoQualificacao;
	}
	public void setBenestare(EnumAprovado benestare) {
		this.benestare = benestare;
	}
	public void setProvasInterrogativas(EnumAprovado provasInterrogativas) {
		this.provasInterrogativas = provasInterrogativas;
	}
}
