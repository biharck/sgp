package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Cep;
import org.nextframework.types.Cnpj;
import org.nextframework.types.InscricaoEstadual;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="empresa")
public class Empresa extends BeanAuditoria{

	private String razaoSocial;
	private String nomeFantasia;
	private Arquivo logomarca;
	private Cnpj cnpj;
	private String inscricaoMunicipal;
	private InscricaoEstadual inscricaoEstadual;
	private Cep cep;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private Municipio municipio;
	private Uf uf;
	private Telefone telefone1;
	private Telefone telefone2;
	private Telefone celular;
	private String email;
	private String emailAlternativo;
	private String twitter;
	private String webSite;
	
	@Required
	@MaxLength(150)
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@Required
	@MaxLength(150)
	@DescriptionProperty
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@ManyToOne
	public Arquivo getLogomarca() {
		return logomarca;
	}
	@Required
	public Cnpj getCnpj() {
		return cnpj;
	}
	@Required
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	public InscricaoEstadual getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	public Cep getCep() {
		return cep;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	@ManyToOne
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	@Transient
	@Required
	public Uf getUf() {
		return uf;
	}
	@Required
	public Telefone getTelefone1() {
		return telefone1;
	}
	public Telefone getTelefone2() {
		return telefone2;
	}
	public Telefone getCelular() {
		return celular;
	}
	@Required
	@Email
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	public String getEmailAlternativo() {
		return emailAlternativo;
	}
	public String getTwitter() {
		return twitter;
	}
	public String getWebSite() {
		return webSite;
	}
	
	//SET
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setLogomarca(Arquivo logomarca) {
		this.logomarca = logomarca;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setInscricaoEstadual(InscricaoEstadual inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEmailAlternativo(String emailAlternativo) {
		this.emailAlternativo = emailAlternativo;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}
