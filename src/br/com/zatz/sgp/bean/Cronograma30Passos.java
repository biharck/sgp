package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="cronograma30passos")
public class Cronograma30Passos extends BeanAuditoria {

	private Integer ordem;
	private String nome;
	private Cronograma30Passos cronogramaPai;
	private boolean paiDeAlguem;

	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@ManyToOne
	@ForeignKey(name="fk_cronograma_pai")
	public Cronograma30Passos getCronogramaPai() {
		return cronogramaPai;
	}
	@Transient
	public boolean isPaiDeAlguem() {
		return paiDeAlguem;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCronogramaPai(Cronograma30Passos cronogramaPai) {
		this.cronogramaPai = cronogramaPai;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setPaiDeAlguem(boolean paiDeAlguem) {
		this.paiDeAlguem = paiDeAlguem;
	}
}
