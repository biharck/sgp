package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="posicaodesenho")
public class PosicaoDesenho extends BeanAuditoria{

	private Integer posicao;
	private String denominacao;
	private String numeroDistForc;
	private String material;
	private double espessura;
	private String classe;
	private String capAppl;
	private String tratt;
	private String npCap;
	private Componente componente;
	
	@Required
	public Integer getPosicao() {
		return posicao;
	}
	
	public String getDenominacao() {
		return denominacao;
	}
	public String getNumeroDistForc() {
		return numeroDistForc;
	}
	@Required
	public String getMaterial() {
		return material;
	}
	public double getEspessura() {
		return espessura;
	}
	public String getClasse() {
		return classe;
	}
	public String getCapAppl() {
		return capAppl;
	}
	public String getTratt() {
		return tratt;
	}
	public String getNpCap() {
		return npCap;
	}
	@ManyToOne
	@ForeignKey(name="fk_posicao_componente")
	public Componente getComponente() {
		return componente;
	}
	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}
	public void setDenominacao(String denominacao) {
		this.denominacao = denominacao;
	}
	public void setNumeroDistForc(String numeroDistForc) {
		this.numeroDistForc = numeroDistForc;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public void setEspessura(double espessura) {
		this.espessura = espessura;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public void setCapAppl(String capAppl) {
		this.capAppl = capAppl;
	}
	public void setTratt(String tratt) {
		this.tratt = tratt;
	}
	public void setNpCap(String npCap) {
		this.npCap = npCap;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
}
