package br.com.zatz.sgp.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="desvio")
public class Desvio extends BeanAuditoria{
	
	private String numero;
	private Projeto projeto;
	private Componente componente;
	private List<Componente> desenhos;
	private String denominacao;
	
	private EnumAprovado pendenciaTecnica;
	private String numeroPendenciaTecnica;
	private Arquivo anexoPendenciaTecnica;
	
	private EnumAprovado provasInterrogativas;
	private String numeroProvasInterrogativas;
	private Arquivo anexoProvasInterrogativas;
	
	private EnumAprovado autoQualificacao;
	private String numeroAutoQualificacao;
	private Arquivo anexoAutoQualificacao;
	
	private EnumAprovado dimensional;
	private String numeroDimensional;
	private Arquivo anexoDimensional;
	
	private EnumAprovado funcionalConfiab;
	private String numeroFuncionalConfiab;
	private Arquivo anexoFuncionalConfiab;
	
	private EnumAprovado estetico;
	private String numeroEstetico;
	private Arquivo anexoEstetico;
	
	private EnumAprovado material;
	private String numeroMaterial;
	private Arquivo anexoMaterial;
	
	private EnumAprovado procFornec;
	private String numeroProcFornec;
	private Arquivo anexoProcFornec;
	
	private EnumAprovado benestare;
	
	private String motivoDesvio;
	private String causaDesvio;
	private String planoAcao;
	private Date inicioLimiteAceito;
	private Date fimLimiteAceito;
	private String qtdLimitePecas;
	private String rastreabilidade;
	private Date validadoEQF;
	private Date amostraEntregue;
	
	private EnumAprovado status;
	private String numeroStatus;
	private Arquivo anexoStatus;
	
	private List<DesvioComponente> desvioComponente;

	@Required
	@DescriptionProperty
	public String getNumero() {
		return numero;
	}
	@Transient
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	@Required
	public Componente getComponente() {
		return componente;
	}
	@Transient
	@Required
	public List<Componente> getDesenhos() {
		return desenhos;
	}
	@Required
	public String getDenominacao() {
		return denominacao;
	}
	
	@Required
	public EnumAprovado getPendenciaTecnica() {
		return pendenciaTecnica;
	}
	public String getNumeroPendenciaTecnica() {
		return numeroPendenciaTecnica;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexopt")
	public Arquivo getAnexoPendenciaTecnica() {
		return anexoPendenciaTecnica;
	}
	public EnumAprovado getProvasInterrogativas() {
		return provasInterrogativas;
	}
	public String getNumeroProvasInterrogativas() {
		return numeroProvasInterrogativas;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexopi")
	public Arquivo getAnexoProvasInterrogativas() {
		return anexoProvasInterrogativas;
	}
	public EnumAprovado getAutoQualificacao() {
		return autoQualificacao;
	}
	public String getNumeroAutoQualificacao() {
		return numeroAutoQualificacao;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexoqua")
	public Arquivo getAnexoAutoQualificacao() {
		return anexoAutoQualificacao;
	}
	public EnumAprovado getDimensional() {
		return dimensional;
	}
	public String getNumeroDimensional() {
		return numeroDimensional;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexodime")
	public Arquivo getAnexoDimensional() {
		return anexoDimensional;
	}
	public EnumAprovado getFuncionalConfiab() {
		return funcionalConfiab;
	}
	public String getNumeroFuncionalConfiab() {
		return numeroFuncionalConfiab;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexofuncconf")
	public Arquivo getAnexoFuncionalConfiab() {
		return anexoFuncionalConfiab;
	}
	public EnumAprovado getEstetico() {
		return estetico;
	}
	public String getNumeroEstetico() {
		return numeroEstetico;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexoest")
	public Arquivo getAnexoEstetico() {
		return anexoEstetico;
	}
	public EnumAprovado getMaterial() {
		return material;
	}
	public String getNumeroMaterial() {
		return numeroMaterial;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexomat")
	public Arquivo getAnexoMaterial() {
		return anexoMaterial;
	}
	public EnumAprovado getProcFornec() {
		return procFornec;
	}
	public String getNumeroProcFornec() {
		return numeroProcFornec;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexoprocforn")
	public Arquivo getAnexoProcFornec() {
		return anexoProcFornec;
	}
	public EnumAprovado getBenestare() {
		return benestare;
	}
	public String getMotivoDesvio() {
		return motivoDesvio;
	}
	public String getCausaDesvio() {
		return causaDesvio;
	}
	public String getPlanoAcao() {
		return planoAcao;
	}
	public Date getInicioLimiteAceito() {
		return inicioLimiteAceito;
	}
	public Date getFimLimiteAceito() {
		return fimLimiteAceito;
	}
	public String getQtdLimitePecas() {
		return qtdLimitePecas;
	}
	public String getRastreabilidade() {
		return rastreabilidade;
	}
	public Date getValidadoEQF() {
		return validadoEQF;
	}
	public Date getAmostraEntregue() {
		return amostraEntregue;
	}
	public EnumAprovado getStatus() {
		return status;
	}
	public String getNumeroStatus() {
		return numeroStatus;
	}
	@ManyToOne
	@ForeignKey(name="fk_desvio_anexostatus")
	public Arquivo getAnexoStatus() {
		return anexoStatus;
	}
	@OneToMany(mappedBy="desvio")
	public List<DesvioComponente> getDesvioComponente() {
		return desvioComponente;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
	public void setDesenhos(List<Componente> desenhos) {
		this.desenhos = desenhos;
	}
	public void setDenominacao(String denominacao) {
		this.denominacao = denominacao;
	}
	public void setPendenciaTecnica(EnumAprovado pendenciaTecnica) {
		this.pendenciaTecnica = pendenciaTecnica;
	}
	public void setNumeroPendenciaTecnica(String numeroPendenciaTecnica) {
		this.numeroPendenciaTecnica = numeroPendenciaTecnica;
	}
	public void setAnexoPendenciaTecnica(Arquivo anexoPendenciaTecnica) {
		this.anexoPendenciaTecnica = anexoPendenciaTecnica;
	}
	public void setProvasInterrogativas(EnumAprovado provasInterrogativas) {
		this.provasInterrogativas = provasInterrogativas;
	}
	public void setNumeroProvasInterrogativas(String numeroProvasInterrogativas) {
		this.numeroProvasInterrogativas = numeroProvasInterrogativas;
	}
	public void setAnexoProvasInterrogativas(Arquivo anexoProvasInterrogativas) {
		this.anexoProvasInterrogativas = anexoProvasInterrogativas;
	}
	public void setAutoQualificacao(EnumAprovado autoQualificacao) {
		this.autoQualificacao = autoQualificacao;
	}
	public void setNumeroAutoQualificacao(String numeroAutoQualificacao) {
		this.numeroAutoQualificacao = numeroAutoQualificacao;
	}
	public void setAnexoAutoQualificacao(Arquivo anexoAutoQualificacao) {
		this.anexoAutoQualificacao = anexoAutoQualificacao;
	}
	public void setDimensional(EnumAprovado dimensional) {
		this.dimensional = dimensional;
	}
	public void setNumeroDimensional(String numeroDimensional) {
		this.numeroDimensional = numeroDimensional;
	}
	public void setAnexoDimensional(Arquivo anexoDimensional) {
		this.anexoDimensional = anexoDimensional;
	}
	public void setFuncionalConfiab(EnumAprovado funcionalConfiab) {
		this.funcionalConfiab = funcionalConfiab;
	}
	public void setNumeroFuncionalConfiab(String numeroFuncionalConfiab) {
		this.numeroFuncionalConfiab = numeroFuncionalConfiab;
	}
	public void setAnexoFuncionalConfiab(Arquivo anexoFuncionalConfiab) {
		this.anexoFuncionalConfiab = anexoFuncionalConfiab;
	}
	public void setEstetico(EnumAprovado estetico) {
		this.estetico = estetico;
	}
	public void setNumeroEstetico(String numeroEstetico) {
		this.numeroEstetico = numeroEstetico;
	}
	public void setAnexoEstetico(Arquivo anexoEstetico) {
		this.anexoEstetico = anexoEstetico;
	}
	public void setMaterial(EnumAprovado material) {
		this.material = material;
	}
	public void setNumeroMaterial(String numeroMaterial) {
		this.numeroMaterial = numeroMaterial;
	}
	public void setAnexoMaterial(Arquivo anexoMaterial) {
		this.anexoMaterial = anexoMaterial;
	}
	public void setProcFornec(EnumAprovado procFornec) {
		this.procFornec = procFornec;
	}
	public void setNumeroProcFornec(String numeroProcFornec) {
		this.numeroProcFornec = numeroProcFornec;
	}
	public void setAnexoProcFornec(Arquivo anexoProcFornec) {
		this.anexoProcFornec = anexoProcFornec;
	}
	public void setMotivoDesvio(String motivoDesvio) {
		this.motivoDesvio = motivoDesvio;
	}
	public void setCausaDesvio(String causaDesvio) {
		this.causaDesvio = causaDesvio;
	}
	public void setPlanoAcao(String planoAcao) {
		this.planoAcao = planoAcao;
	}
	public void setInicioLimiteAceito(Date inicioLimiteAceito) {
		this.inicioLimiteAceito = inicioLimiteAceito;
	}
	public void setFimLimiteAceito(Date fimLimiteAceito) {
		this.fimLimiteAceito = fimLimiteAceito;
	}
	public void setQtdLimitePecas(String qtdLimitePecas) {
		this.qtdLimitePecas = qtdLimitePecas;
	}
	public void setRastreabilidade(String rastreabilidade) {
		this.rastreabilidade = rastreabilidade;
	}
	public void setValidadoEQF(Date validadoEQF) {
		this.validadoEQF = validadoEQF;
	}
	public void setAmostraEntregue(Date amostraEntregue) {
		this.amostraEntregue = amostraEntregue;
	}
	public void setStatus(EnumAprovado status) {
		this.status = status;
	}
	public void setNumeroStatus(String numeroStatus) {
		this.numeroStatus = numeroStatus;
	}
	public void setAnexoStatus(Arquivo anexoStatus) {
		this.anexoStatus = anexoStatus;
	}
	public void setDesvioComponente(List<DesvioComponente> desvioComponente) {
		this.desvioComponente = desvioComponente;
	}
	public void setBenestare(EnumAprovado benestare) {
		this.benestare = benestare;
	}
}
