package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="desviocomponente")
public class DesvioComponente extends BeanAuditoria {

	private Desvio desvio;
	private Componente componente;
	
	
	public DesvioComponente(){}
	
	public DesvioComponente(Desvio desvio, Componente componente){
		this.desvio = desvio;
		this.componente = componente;
	}
	
	@ManyToOne
	@ForeignKey(name="fk_desviocomponente_desvio")
	public Desvio getDesvio() {
		return desvio;
	}
	@ManyToOne
	@ForeignKey(name="fk_desviocomponente_comp")
	public Componente getComponente() {
		return componente;
	}
	public void setDesvio(Desvio desvio) {
		this.desvio = desvio;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
}
