package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um papel no sistema, um n�vel de permiss�o.
 * Ex.: Administrador, Usu�rio, Financeiro
 */
@Entity
@Table(name="papel")
public class Papel implements org.nextframework.authorization.Role {
    
	private Integer id;
	private String descricao;
	private String nome;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	//TRANSIENT
	@Transient
	public String getName() {
		return getNome();
	}
	
	//GET
	@Required
	@MaxLength(value=100)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@Required
	@MaxLength(value=50)
	public String getNome() {
		return nome;
	}

	//SET
	public void setId(Integer id) {
		this.id = id;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}