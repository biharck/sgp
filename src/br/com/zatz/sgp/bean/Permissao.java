package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.authorization.Role;
import org.nextframework.authorization.impl.AbstractPermission;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

/**
 * Representa a permiss�o para determinado papel em determinada tela
 */
@Entity
@Table(name="permissao")
public class Permissao extends AbstractPermission {
    
    private Long id;
    private Papel papel;
    private Tela tela;
    private String permissionString;
    
    //TRANSIENT
    @Transient
    public Role getRole() {
    	return getPapel();
    }
    
    //GET
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	@DescriptionProperty
	@Required
	@ManyToOne
	@ForeignKey(name="fk_permissao_tela")
	public Tela getTela() {
		return tela;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_permissao_papel")
	public Papel getPapel() {
		return papel;
	}
	@Override
	public String getPermissionString() {
		return permissionString;
	}
	@Override
	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}

	//SET
	public void setTela(Tela tela) {
		this.tela = tela;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public void setId(Long id) {
		this.id = id;
	}
}