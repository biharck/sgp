package br.com.zatz.sgp.bean;

import java.sql.Timestamp;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BeanAuditoria {

	/**
    * 
    */
	private static final long serialVersionUID = 5107809834151625347L;

	protected Integer id;
	private String userInc;
	private Timestamp timeInc;
	private String userAlt;
	private Timestamp timeAlt;
	private String userDel;
	private Timestamp timeDel;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public String getUserInc() {
		return userInc;
	}
	public Timestamp getTimeInc() {
		return timeInc;
	}
	public String getUserAlt() {
		return userAlt;
	}
	public Timestamp getTimeAlt() {
		return timeAlt;
	}
	public String getUserDel() {
		return userDel;
	}
	public Timestamp getTimeDel() {
		return timeDel;
	}
	
	//SET
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUserInc(String userInc) {
		this.userInc = userInc;
	}
	public void setTimeInc(Timestamp timeInc) {
		this.timeInc = timeInc;
	}
	public void setUserAlt(String userAlt) {
		this.userAlt = userAlt;
	}
	public void setTimeAlt(Timestamp timeAlt) {
		this.timeAlt = timeAlt;
	}
	public void setUserDel(String userDel) {
		this.userDel = userDel;
	}
	public void setTimeDel(Timestamp timeDel) {
		this.timeDel = timeDel;
	}
}