package br.com.zatz.sgp.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="cronograma30passoscomponente")
public class Cronograma30PassosComponente extends BeanAuditoria{
	
	private Componente componente;
	private Cronograma30Passos cronograma30Passos;
	private Date inicio;
	private Date termino;

	@ManyToOne
	@ForeignKey(name="fk_componente_c30pc")
	public Componente getComponente() {
		return componente;
	}
	@ManyToOne
	@ForeignKey(name="fk_c30p_c30pc")
	public Cronograma30Passos getCronograma30Passos() {
		return cronograma30Passos;
	}
	public Date getInicio() {
		return inicio;
	}
	public Date getTermino() {
		return termino;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
	public void setCronograma30Passos(Cronograma30Passos cronograma30Passos) {
		this.cronograma30Passos = cronograma30Passos;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public void setTermino(Date termino) {
		this.termino = termino;
	}
}
