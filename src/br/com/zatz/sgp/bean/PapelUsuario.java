package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

/**
 * Faz o relacionamento entre Usu�rio e Papel
 * Relacionamento muitos para muitos entre usuario e papel
 */
@Entity
@Table(name="papelusuario")
public class PapelUsuario{

	private Integer id;
	private Usuario usuario;
	private Papel papel;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_papeluser_user")
    public Usuario getUsuario() {
		return usuario;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_papeluser_papel")
	public Papel getPapel() {
		return papel;
	}

	//SET
	public void setId(Integer id) {
		this.id = id;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
