package br.com.zatz.sgp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.types.File;

@Entity
@Table(name="arquivo")
public class Arquivo implements File {

	Long cdfile;
	String name;
	String contenttype;
	Long size;
	byte[] content;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Override
	public Long getCdfile() {
		return cdfile;
	}
	

	public String getName() {
		return name;
	}

	public String getContenttype() {
		return contenttype;
	}

	public Long getSize() {
		return size;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public byte[] getContent() {
		return content;
	}


	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setCdfile(Long cdfile) {
		this.cdfile = cdfile;
	}


}