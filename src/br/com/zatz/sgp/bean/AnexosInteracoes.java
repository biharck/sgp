package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="anexosinteracoes")
public class AnexosInteracoes extends BeanAuditoria{
	
	private Arquivo anexo;
	private Interacoes interacoes;
	
	@ManyToOne
	@ForeignKey(name="fk_anexosint_arquivo")
	public Arquivo getAnexo() {
		return anexo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@ForeignKey(name="fk_anexosint_interacoes")
	public Interacoes getInteracoes() {
		return interacoes;
	}
	public void setAnexo(Arquivo anexo) {
		this.anexo = anexo;
	}
	public void setInteracoes(Interacoes interacoes) {
		this.interacoes = interacoes;
	}
}
