package br.com.zatz.sgp.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="scheda")
public class Scheda extends BeanAuditoria {
	
	private String descricao;	
	private String numero;	
	private Date dataSolicitacao;
	private Money investimento;
	private String projectChief;
	private List<Interacoes> interacoes;
	private Status status;
	private Interacoes interacoesTransient;
	private List<SchedaComponente> schedaComponente;

	private Projeto projeto;
	private List<Componente> desenhos;
	
	private Componente componente;
	@Transient
	@Required
	public Componente getComponente() {
		return componente;
	}
	public void setComponente(Componente componente) {
		this.componente = componente;
	}
	
	@DescriptionProperty
	@Required
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@Required
	public String getNumero() {
		return numero;
	}
	@Required
	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}
	public Money getInvestimento() {
		return investimento;
	}
	@Required
	public String getProjectChief() {
		return projectChief;
	}
	@OneToMany(mappedBy="scheda")
	public List<Interacoes> getInteracoes() {
		return interacoes;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_scheda_status")
	public Status getStatus() {
		return status;
	}
	@Transient
	public Interacoes getInteracoesTransient() {
		return interacoesTransient;
	}
	@OneToMany(mappedBy="scheda")
	public List<SchedaComponente> getSchedaComponente() {
		return schedaComponente;
	}
	@Transient
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	@Required
	public List<Componente> getDesenhos() {
		return desenhos;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}
	public void setProjectChief(String projectChief) {
		this.projectChief = projectChief;
	}
	public void setInvestimento(Money investimento) {
		this.investimento = investimento;
	}
	public void setInteracoes(List<Interacoes> interacoes) {
		this.interacoes = interacoes;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public void setInteracoesTransient(Interacoes interacoesTransient) {
		this.interacoesTransient = interacoesTransient;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDesenhos(List<Componente> desenhos) {
		this.desenhos = desenhos;
	}
	public void setSchedaComponente(List<SchedaComponente> schedaComponente) {
		this.schedaComponente = schedaComponente;
	}
}
