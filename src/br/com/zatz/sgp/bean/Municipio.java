package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="municipio")
public class Municipio extends BeanAuditoria  {
    
	private String nome; 
	private Uf uf;
	
	//GET
	@DescriptionProperty
    @Required
    @MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_municipio_uf")
	public Uf getUf() {
		return uf;
	}	
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
}