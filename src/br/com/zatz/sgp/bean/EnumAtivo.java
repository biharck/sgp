package br.com.zatz.sgp.bean;

public enum EnumAtivo {

	ATIVO("Ativo",'S',true),
	INATIVO("Inativo", 'N',false)
	;
	
	private EnumAtivo(String nome, char opcao,boolean optBool){
		this.nome = nome;
		this.opcao = opcao;
		this.optBool = optBool;
	}
	
	private String nome;
	private char opcao;
	private boolean optBool;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public boolean isOptBool() {
		return optBool;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
