package br.com.zatz.sgp.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="unidademedida")
public class UnidadeMedida extends BeanAuditoria  {
    
	private String nome; 
	
	
	//GET
	@DescriptionProperty
    @Required
    @MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}