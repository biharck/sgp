package br.com.zatz.sgp.bean;

public enum EnumAprovado {

	APROVADO("Aprovado",'A',true),
	REPROVADO("Reprovado", 'R',false),
	NAO_APLICADO("N�o Aplicado", 'N',null)
	;
	
	private EnumAprovado(String nome, char opcao,Boolean optBool){
		this.nome = nome;
		this.opcao = opcao;
		this.optBool = optBool;
	}
	
	private String nome;
	private char opcao;
	private Boolean optBool;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public Boolean isOptBool() {
		return optBool;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
