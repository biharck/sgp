package br.com.zatz.sgp.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.authorization.User;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um usu�rio no sistema
 */
@Entity
@Table(name="usuario")
public class Usuario extends BeanAuditoria implements User {
    
	private String nome;
	private String login;
	private String senha;
	private String reSenha;
	private String email;
	private String reEmail;
	private boolean ativo;
	private boolean senhaProvisoria;
	private Timestamp dtUltimoLogin;
	private String senhaAnterior;
	private String senhaAnterior2;
	private List<Papel> papeis;
	private List<PapelUsuario> papeisUsuario; 
	
	
	//TRANSIENT
	@Transient
	public String getPassword() {
		return getSenha();
	}
	@Required
	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}
	//GET
	@DescriptionProperty
    @Required
    @MaxLength(value=50)
	public String getNome() {
		return nome;
	}
	@Required
	public String getLogin() {
		return login;
	}
	@Password
	@Required
	@MaxLength(value=20)
	public String getSenha() {
		return senha;
	}
	@Password
	@Required
	@Transient
	public String getReSenha() {
		return reSenha;
	}
	@Email
	@Required
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@Transient
	public String getReEmail() {
		return reEmail;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public boolean isSenhaProvisoria() {
		return senhaProvisoria;
	}
	public Timestamp getDtUltimoLogin() {
		return dtUltimoLogin;
	}
	@Password
	public String getSenhaAnterior() {
		return senhaAnterior;
	}
	@Password
	public String getSenhaAnterior2() {
		return senhaAnterior2;
	}
	@OneToMany(mappedBy="usuario")
	public List<PapelUsuario> getPapeisUsuario() {
		return papeisUsuario;
	}
	
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public void setReSenha(String reSenha) {
		this.reSenha = reSenha;
	}
	public void setSenhaProvisoria(boolean senhaProvisoria) {
		this.senhaProvisoria = senhaProvisoria;
	}
	public void setDtUltimoLogin(Timestamp dtUltimoLogin) {
		this.dtUltimoLogin = dtUltimoLogin;
	}
	public void setSenhaAnterior(String senhaAnterior) {
		this.senhaAnterior = senhaAnterior;
	}
	public void setSenhaAnterior2(String senhaAnterior2) {
		this.senhaAnterior2 = senhaAnterior2;
	}
	public void setPapeisUsuario(List<PapelUsuario> papeisUsuario) {
		this.papeisUsuario = papeisUsuario;
	}	
	
	
}