package br.com.zatz.sgp.autenticacao.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.authorization.User;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.nextframework.view.menu.MenuTag;
import org.springframework.web.servlet.ModelAndView;

import br.com.zatz.sgp.bean.Empresa;
import br.com.zatz.sgp.bean.Usuario;
import br.com.zatz.sgp.dao.ArquivoDAO;
import br.com.zatz.sgp.dao.NextAuthorizationDAO;
import br.com.zatz.sgp.service.EmpresaService;
import br.com.zatz.sgp.service.UsuarioService;
import br.com.zatz.sgp.util.PathUtil;

/**
 * Controller que far� o login do usu�rio na aplica��o.
 * Se o login for efetuado com sucesso ir� redirecionar para /secured/Index
 */
@Controller(path="/autenticacao/Login")
public class LoginController extends MultiActionController {

	
	NextAuthorizationDAO authorizationDAO;
	private EmpresaService empresaService;
	private ArquivoDAO arquivoDAO;
	
	public void setAuthorizationDAO(NextAuthorizationDAO authorizationDAO) {
		this.authorizationDAO = authorizationDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	/**
	 * Action que envia para a p�gina de login
	 */
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request, Usuario usuario){
		return new ModelAndView("login", "usuario", usuario);
	}
	
	/**
	 * Efetua o login do usu�rio
	 */
	public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
		String login = usuario.getLogin();
		//se foi passado o login na requisi��o, iremos verificar se o usu�rio existe e a senha est� correta
		if(login != null){
			//buscamos o usu�rio do banco pelo login
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			
			User userByLogin = authorizationDAO.findUserByLogin(login);
			
			// se o usu�rio existe e a senha est� correta
			if(userByLogin != null && encryptor.checkPassword(usuario.getPassword(), userByLogin.getPassword())){
				//Setando o atributo de se��o USER fazemos o login do usu�rio no sistema. 
				request.setUserAttribute("USER", userByLogin);

				//Limpamos o cache de permiss�es o menu.
				//O menu ser� refeito levando em considera��o as permiss�es do usu�rio
				request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
				
				Usuario usuarioLogado = UsuarioService.getInstance().loadForEntrada((Usuario) userByLogin);
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:MM:ss");				
				request.getSession().setAttribute("dtUltimoLogin", format.format(new Date(usuarioLogado.getDtUltimoLogin().getTime())));
				request.getSession().setAttribute("nomeExibicao", usuarioLogado.getNome());
				
				//verifica se usu�rio deve alterar senha
				if(usuarioLogado.isSenhaProvisoria()){
					request.getSession().setAttribute("isSenhaProvisoria", true);
				}else
					request.getSession().setAttribute("isSenhaProvisoria", false);

				
				//altera ultimo login realizado
				UsuarioService.getInstance().alteraDtUltimoLogin(usuarioLogado);
				Locale locale = new Locale("pt", "BR"); 
		    	// define o Locale padr�o da JVM
		    	Locale.setDefault(locale);
		    	
		    	Empresa emp = empresaService.getEmpresa();
		    	if(emp!=null && emp.getLogomarca()!=null && emp.getLogomarca().getCdfile()!=null){
			    	arquivoDAO.loadAsImage(emp.getLogomarca());
					DownloadFileServlet.addCdfile(request.getSession(), emp.getLogomarca().getCdfile());
					request.getSession().setAttribute("empresa", emp);
		    	}
		    	
		    	
				return new ModelAndView("redirect:"+PathUtil.AFTER_LOGIN_GO_TO);				
			}
			
			//Se o login e/ou a senha n�o estiverem corretos, avisar o usu�rio
			request.addMessage("Login e/ou senha inv�lidos", MessageType.ERROR);
		}
		
		//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
		usuario.setSenha(null);
		return doPage(request, usuario);
	}

}