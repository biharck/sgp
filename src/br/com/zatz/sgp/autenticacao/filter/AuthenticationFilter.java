package br.com.zatz.sgp.autenticacao.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.nextframework.core.standard.Next;

public class AuthenticationFilter implements Filter {

	String loginPage = "/autenticacao/Login";

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if(Next.getUser() == null){
			request.getRequestDispatcher(loginPage).forward(request, response);
		} else {
			chain.doFilter(request, response);
		}
	
	}

	public void destroy() {}
	public void init(FilterConfig config) throws ServletException {}

}