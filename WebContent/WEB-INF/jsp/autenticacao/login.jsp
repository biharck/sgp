<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<script type="text/javascript">
	function validaCamposLogin(){
		dialogAguarde();
		login = trim(document.getElementsByName('login')[0].value);
		senha = trim(document.getElementsByName('senha')[0].value);
		erros='';
		if(login==null||login==undefined||login.length==0)
			erros=erros+'<br/>'+'O campo Login � obrigat�rio';
		if(senha==null||senha==undefined||senha.length==0)
			erros=erros+'<br/>'+'O campo Senha � obrigat�rio';
		if(erros.length!=0){
			dialog('Aten��o',erros);
			return false;
		}
		return true;
	}	
</script>
<!-- ui-dialog para mensagens -->
<div id="dialog">
	<p></p>
</div>
<n:form validateFunction="validaCamposLogin">
	<n:bean name="usuario">
		<t:propertyConfig renderAs="double" mode="input">
			<n:panelGrid 
					columns="6" 
					align="center" 
					style=" padding:2px; border: none;margin-top: 300px;" 
					columnStyles="font-size: 12px;">
				<t:property name="login" id="login"/>
				<t:property name="senha" type="password" label="Senha"/>
				<n:submit type="submit" action="doLogin" panelColspan="2" panelAlign="right">Logar</n:submit>
				<n:panel colspan="6">
					<center>Esqueceu sua senha? <a href="javascript:dialog('Esqueceu Sua Senha?','Informe seu email.....')"><font style="color:#1F75CC">Clique aqui!</font></a></center>				
				</n:panel>
			</n:panelGrid>
		</t:propertyConfig>
	</n:bean>
</n:form>
<style type="text/css">
	body{
		background: url('/SGP/img/style/bg-image.png');
	}
</style>
<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#login').focus();
	 });
</script>