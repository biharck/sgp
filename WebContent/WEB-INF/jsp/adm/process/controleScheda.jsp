<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela>
	<n:link type="submit" action="schedasParaVencer">Mostrar Schedas Pendentes</n:link>
	<n:link type="submit" action="sendMailSchedasParaVencer">Enviar E-mail Schedas Pendentes</n:link>
	<br />
	<n:link type="submit" action="schedasVencidas">Mostrar Schedas Vencidas</n:link>
	<n:link type="submit" action="sendMailSchedasVencidas">Enviar E-mail Schedas Vencidas</n:link>
	<n:dataGrid itens="schedas" var="sc" itemType="br.com.zatz.sgp.bean.Scheda" cellspacing="0"
		rowonclick="javascript:$csu.coloreLinha('tabelaResultados',this)"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
		<t:property name="numero" mode="output"/>
		<t:property name="projectChief" mode="output"/>
		<t:property name="descricao" label="T�tulo" mode="output"/>
		<n:column header="Cria��o">
			<t:property name="timeInc" mode="output" />
		</n:column>
		<n:column header="Altera��o">
			<t:property name="timeAlt" mode="output"/>
		</n:column>
		<t:property name="status" mode="output"/>
		<n:column header="A��o">
			<a rel="modalFrame" href="/SGP/adm/crud/Scheda?ACAO=editar&showMenu=false&id=${sc.id}" class="tootip" title='Clique aqui para visualizar esta Scheda. Voc� tamb�m poder� adiconar um nova intera��o para esta Scheda.' title-modal="Tela de Edi��o da Scheda do Componente ${componente.nome}">consultar</a>
		</n:column>
	</n:dataGrid>
	<script type="text/javascript">
		$(function() {
			$('a[rel=modalFrame]').click(function(e) {
				e.preventDefault();
				var $this = $(this);
				var horizontalPadding = 30;
				var verticalPadding = 30;
		        $('<iframe id="cadScheda" class="externalSite" src="' + this.href + '" />').dialog({
		            title: ($this.attr('title-modal')) ? $this.attr('title-modal') : '',
		            autoOpen: true,
		            width: 980,
		            height: 630,
		            modal: true,
		            resizable: true,
					autoResize: true,
		            overlay: {
		                opacity: 0.5,
		                background: "black"
		            }
		        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
			});
		});
		function closeIframe(){
		   var iframe = document.getElementById('cadScheda');
		   iframe.parentNode.removeChild(iframe);
		   dialog('Aten��o','Aguarde um instante por favor...')
		   if(location.href.indexOf('ACAO') == -1){//significa que � um registro novo
		   		if(location.href.indexOf('Desvio') == -1)
			   		location.href = location.href+"?ACAO=editar&id=${componente.id}&janelaEntrada=5";
			   	else
			   		location.href = location.href+"?ACAO=editar&id=${componente.id}&janelaEntrada=4";
		   }else
		   		if(location.href.indexOf('Desvio') == -1)
		   			location.href = location.href+"&janelaEntrada=5";
		   		else
		   			location.href = location.href+"&janelaEntrada=4";
		}
		function closeIframeWithoutReload(){
			closeIframe();
		}
	</script>
</t:tela>
