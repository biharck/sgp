<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
 <span>Alterar Senha</span>
	<n:form  action="alteraSenha" method="post" validateFunction="validaIgualdade">
		<t:janelaFiltro>
			<t:tabelaFiltro showSubmit="false">
	         <n:panel>
              <n:panelGrid columns="2" >			
                <t:property name="senhaAtual" renderAs="doubleline" id="senhaAtual" colspan="2"/>
				<t:property name="senhaNova" renderAs="doubleline" id="senhaNova" onchange="validaTamMinimoCampo($('#senhaNova'),'nova senha',4)"/>
				<t:property name="reSenhaNova" renderAs="doubleline" id="reSenhaNova" onchange="validaIgualdadeCampo($('#senhaNova'),this,'nova senha')" />
			  </n:panelGrid>
             </n:panel> 
            </t:tabelaFiltro>
		</t:janelaFiltro>
		<n:submit type="submit" action="doSalvar">Salvar</n:submit>
	</n:form>


<script type="text/javascript">
  $(document).ready(function() {
   $('#senhaAtual').focus();
  });
 
  
   function validaCampos(){
    dialogAguarde();
    senhaAtual = trim(document.getElementsByName('senhaAtual')[0].value);
    erros='';
    if(senhaAtual==null||senhaAtual==undefined||senhaAtual.length==0)
     erros=erros+'<br/>'+'O campo senha atual � obrigat�rio';
    if(erros.length!=0){
     dialog('Aten��o',erros);
     return false;
    }
    return true;
   } 

  function validaIgualdade(){
    return(validaTamMinimoCampo($('#senhaNova'),'nova senha',4) &&
           validaIgualdadeCampo($('#senhaNova'),$('#reSenhaNova'),'nova senha') &&
           validaCampos());
  }
</script>