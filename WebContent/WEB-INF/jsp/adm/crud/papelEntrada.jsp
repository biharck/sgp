<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false"/>
					<t:property name="nome" id="nome"  renderAs="doubleline" />
					<t:property name="descricao" style="width:400px" renderAs="doubleline"/>
					<!-- apenas para teste, ap�s teste apagar -->
					<c:if test="${!empty papel.id}">
						<div id="div-ajax"></div>
						<n:panel>
							<a href="javascript:ajaxTeste(${papel.id},'outro parametro qualquer')">Teste de ajax</a>
						</n:panel>
					</c:if>
					<!-- fim teste -->
				</n:panelGrid>
			</n:panel>	
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>


<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>

