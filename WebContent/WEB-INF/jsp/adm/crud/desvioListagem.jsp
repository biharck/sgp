<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem >
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel >
				<n:panelGrid columns="4">
					<t:property name="numero" colspan="2"/>
					<t:property name="denominacao" colspan="2"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="projeto" colspan="2"/>
						<t:property name="componente" itens="componenteDAO.findComponentesByProjeto(projeto)" colspan="2"/>
					</n:comboReloadGroup>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="numero"/>
			<t:property name="denominacao"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>