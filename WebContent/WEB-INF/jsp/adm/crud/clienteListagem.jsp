<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nomeFantasia" style="width:400px;"/>
			<t:property name="cnpj"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<n:column header="Foto" width="40">  
                <c:if test="${!empty cliente.logomarca}">  
                    <img  src="${application}/DOWNLOADFILE/${cliente.logomarca.cdfile}" width="32" id="imagem_listagem" class="foto"/><!-- <t:property name="logomarca"/> -->  
                </c:if>  
            </n:column>  
			<t:property name="nomeFantasia"/>
			<t:property name="cnpj"/>
			<t:property name="razaoSocial"/>
			<t:property name="inscricaoMunicipal"/>
			<t:property name="inscricaoEstadual"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>