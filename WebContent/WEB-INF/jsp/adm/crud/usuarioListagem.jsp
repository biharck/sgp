<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome" style="width:400px;"/>
			<t:property name="ativo"/>
			<n:panel valign="top" style="padding-top:4px">Papeis</n:panel>
			<n:dataGrid itemType="br.com.zatz.sgp.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
				<n:column width="20">
                    <n:input name="papeis" value="${row}" type="checklist" itens="${usuario.papeis}"/>
                </n:column>
                <n:column>
	                <t:property name="nome" mode="output" label=" "/>
                </n:column>
			</n:dataGrid>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="${empty param.hideOptions}">
			<t:property name="nome"/>
			<t:property name="login"/>
			<t:property name="ativo"/>
			<n:column header="Papel">
	            <c:forEach items="${usuario.papeisUsuario}" var="bean">
	            	<c:out value="${bean.papel.nome}"></c:out><br/>
	            </c:forEach>
			</n:column>
			<t:acao>
				&nbsp;
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

