<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:entrada>
	<t:janelaEntrada>
			<t:tabelaEntrada  colspan="2">
				<n:panelGrid columns="6">
					<t:property name="cnpj" renderAs="doubleline" id="cnpj" colspan="6" style="width:150px;"/>
					<t:property name="razaoSocial"  renderAs="doubleline" colspan="6" style="width:500px"/>
					<t:property name="nomeFantasia" renderAs="doubleline" colspan="6" style="width:500px"/>
					<t:property name="inscricaoMunicipal" renderAs="doubleline" colspan="3" style="width:250px"/>
					<t:property name="inscricaoEstadual" renderAs="doubleline" colspan="3" style="width:250px"/>
					<t:property name="email" renderAs="doubleline" colspan="6" style="width:500px" />
					<t:property name="emailAlternativo" renderAs="doubleline" colspan="6" style="width:500px"/>
					<t:property name="twitter" renderAs="doubleline" colspan="3" style="width:250px"/>
					<t:property name="webSite" renderAs="doubleline" colspan="3" style="width:250px"/>
				</n:panelGrid>
				<n:panel id="picture" style="text-align:center;">
					<c:if test="${param.ACAO != 'criar'}">
						<c:if test="${showpicture}">
							<n:link url="/DOWNLOADFILE/${cliente.logomarca.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${cliente.logomarca.cdfile}" id="imagem" class="foto"/>
							</n:link>
						</c:if>
						<c:if test="${!showpicture}">
							<div class="image">
								<img src="${pageContext.request.contextPath}/img/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
							</div>
						</c:if>
					</c:if>
					<c:if test="${param.ACAO == 'criar'}">
						<div class="image">
							<img src="${pageContext.request.contextPath}/img/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
						</div>
					</c:if>
					<t:property name="logomarca"  id="foto" label="" renderAs="doubleline" onchange="verifyExt();" />
				</n:panel>
				<n:panel colspan="2">-<span class="titulo">Endere�o</span></n:panel>
				<n:panelGrid columns="6" colspan="2">
					<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" style="width:100px;"/>
					<t:property name="endereco" renderAs="doubleline" colspan="3" style="width:400px;"/>
					<t:property name="numero" renderAs="doubleline" colspan="2"/>
					<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
					<t:property name="bairro" renderAs="doubleline" colspan="1" style="width:200px;"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="uf" renderAs="doubleline" colspan="1" />
						<t:property name="municipio" renderAs="doubleline" colspan="1"/>
					</n:comboReloadGroup>
					<t:property name="id" renderAs="doubleline" type="hidden" showLabel="false" write="false" label=" "/>
				</n:panelGrid>
			</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>


