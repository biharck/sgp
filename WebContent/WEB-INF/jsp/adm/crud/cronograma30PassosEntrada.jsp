<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4" renderAs="doubleline"/>
					<t:property name="ordem" colspan="4" renderAs="doubleline"/>
					<t:property name="nome" id="nome" colspan="4" renderAs="doubleline" style="width:400px;"/>
					<t:property name="cronogramaPai" colspan="2" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script> 