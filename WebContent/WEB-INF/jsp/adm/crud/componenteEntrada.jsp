<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="tldsgp" uri="sgp"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada  colspan="2" title="Detalhes">
			<n:panelGrid columns="2">
				<t:property name="id"/>				
				
				<c:if test="${param.ACAO == 'copiar'}">
					<t:property name="vdVt" selectOnePath="/adm/crud/Componente?showMenu=false"/>
					<t:property name="nome" id="nome" style="width: 200px;" mode="output"/>
					<t:property name="copia" type="hidden" showLabel="false" label=" " write="false"/>
				</c:if>
				<c:if test="${param.ACAO != 'copiar'}">
					<t:property name="nome" id="nome" style="width: 200px;"/>
				</c:if>
				
				<c:if test="${!empty componente.vdVt}">
					<t:property name="vdVt" mode="output"/>
				</c:if>
				
				<t:property name="numero" colspan="2"/>
				<t:property name="descricao" type="TEXT_AREA" cols="27" rows="4" colspan="2"/>
				<c:if test="${param.ACAO == 'copiar'}">
					<t:property name="matricula" colspan="2" mode="output"/>
				</c:if>
				<c:if test="${param.ACAO != 'copiar'}">
					<t:property name="matricula" colspan="2"/>
				</c:if>
				<t:property name="projectChief" colspan="2" selectOnePath="/adm/crud/Usuario?showMenu=false"/>
				<t:property name="massa" colspan="2"/>
				<t:property name="investimento" colspan="2"/>
				<t:property name="custoPeca" colspan="2"/>
				<t:property name="numeroTCAE" colspan="2"/>
				<t:property name="revisao" colspan="2"/>
				<t:property name="benestare" colspan="2"/>
				<t:property name="autoQualificacao" colspan="2"/>
				<t:property name="provasInterrogativas" colspan="2"/>
				<c:if test="${empty componente.id && param.ACAO != 'copiar'}">
					<t:property name="projeto" colspan="2"/>
				</c:if>
				<c:if test="${!empty componente.id || param.ACAO == 'copiar'}">
					<t:property name="projeto" colspan="2" mode="output"/>
				</c:if>				

			</n:panelGrid>
			<n:panelGrid columns="1">
				<n:panel id="picture" colspan="2" style="text-align:center;">
					<c:if test="${param.ACAO != 'criar' && param.ACAO != 'copiar'}">
						<c:if test="${showpicture}">
							<br />
							<n:link url="/DOWNLOADFILE/${componente.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${componente.foto.cdfile}" id="imagem" class="foto"/>
							</n:link>
						</c:if>
						<c:if test="${!showpicture}">
							<div class="image">
								<img src="${pageContext.request.contextPath}/img/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
							</div>
						</c:if>
					</c:if>
					<c:if test="${param.ACAO == 'criar' || param.ACAO == 'copiar'}">
						<div class="image">
							<img src="${pageContext.request.contextPath}/img/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
						</div>							
					</c:if>
					<t:property name="foto" id="foto" label="" renderAs="doubleline" onchange="verifyExt();" />					
				</n:panel>
			</n:panelGrid>			
			<c:if test="${!empty schedasAbertas}">
				<n:panelGrid columns="2">
					<c:if test="${param.ACAO == 'consultar'}">
						<n:panel>
							<h3>
								<p><img src="/SGP/img/warning-icon.png" />Aten��o! Este componente possui <font style="color:#E77272">${schedasAbertas} Scheda(s)</font> em aberto!</p>
							</h3>
						</n:panel>
					</c:if>
				</n:panelGrid>
				<n:panelGrid columns="2">
					<c:if test="${param.ACAO == 'editar'}">
						<n:panel>
							<h3>
								<p><img src="/SGP/img/warning-icon.png" />Aten��o! Este componente possui <font style="color:#E77272">${schedasAbertas} Scheda(s)</font> em aberto!</p>
							</h3>
						</n:panel>
					</c:if>
				</n:panelGrid>
			</c:if>
		</t:tabelaEntrada>
		<t:tabelaEntrada title="Desenhos"  >
			<n:panel colspan="2" style="min-width:400px;">
				<t:detalhe name="anexos"  labelnovalinha="Novo Desenho" >
					<t:property name="id"/>
					<t:property name="anexo" showRemoverButton="false"/>
					<t:acao>
					</t:acao>
				</t:detalhe>
			</n:panel>
		</t:tabelaEntrada>
		<t:tabelaEntrada title="Materiais">
			<n:panel style="min-width:800px;">
				<t:detalhe name="posicoesDesenhos" labelnovalinha="Nova Posi��o">
						<n:column header="Posi��es no Desenho">
							<n:panelGrid columns="6">
								<t:propertyConfig mode="input" renderAs="double" showLabel="true">
									<c:if test="${param.ACAO != 'consultar'}">
										<t:property name="posicao" colspan="2"/>
										<t:property name="denominacao" colspan="2"/>
										<t:property name="numeroDistForc" colspan="2"/>
										<t:property name="material" colspan="2"/>
										<t:property name="espessura" colspan="2"/>
										<t:property name="classe" colspan="2"/>										
										<t:property name="capAppl" colspan="2"/>										
										<t:property name="tratt" colspan="2"/>										
										<t:property name="npCap" colspan="2"/>										
									</c:if>
									<c:if test="${param.ACAO == 'consultar'}">
										<t:property name="posicao" colspan="2" disabled="disabled"/>
										<t:property name="denominacao" colspan="2" disabled="disabled"/>
										<t:property name="numeroDistForc" colspan="2" disabled="disabled"/>
										<t:property name="material" colspan="2" disabled="disabled"/>
										<t:property name="espessura" colspan="2" disabled="disabled"/>
										<t:property name="classe" colspan="2" disabled="disabled"/>			
										<t:property name="capAppl" colspan="2" disabled="disabled"/>					
										<t:property name="tratt" colspan="2" disabled="disabled"/>				
										<t:property name="npCap" colspan="2" disabled="disabled"/>
										
									</c:if>
								</t:propertyConfig>
							</n:panelGrid>
						</n:column>
					</t:detalhe>
			</n:panel>
		</t:tabelaEntrada>
		<c:if test="${!empty componente.id}">
			<t:tabelaEntrada title="Cronograma 30 passos">
				<c:if test="${!empty componente.vdVt}">
					<br/>
					<div>
						<span>
							Este � um componente filho de 
							<font style="color:#E77272">${componente.vdVt.nome}</font><br />
							Para ver o cronograma 30 passos do componente pai, 
							<a rel="modalFrame" href="/SGP/adm/crud/Componente?ACAO=consultar&showMenu=false&id=${componente.vdVt.id}&janelaEntrada=3" title-modal="Componente pai do componente ${componente.nome}">
								clique aqui
							</a>
						</span>
					</div>
					<br/>
					<br/>
					<br/>
				</c:if>
				<c:if test="${empty componente.vdVt}">
					<t:detalhe name="listaCronograma30PassosComponente" showBotaoRemover="false" showBotaoNovaLinha="false">
						<t:property name="cronograma30Passos.ordem" mode="output"/>
						<n:column header="Nome da Tarefa">						
							<c:if test="${!empty cronograma30PassosComponente.cronograma30Passos.cronogramaPai.id}">
								<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${cronograma30PassosComponente.cronograma30Passos.nome}</i>
							</c:if>
							<c:if test="${empty cronograma30PassosComponente.cronograma30Passos.cronogramaPai.id}">
								<b>${cronograma30PassosComponente.cronograma30Passos.nome}</b>
							</c:if>
							
						</n:column>
						
						<n:column header="In�cio">
							<t:property name="inicio" id="inicio_${index}"/>
							<c:if test="${param.ACAO != 'consultar'}">
								<button id="iniciobtn_${index}" class="calendarbutton">...</button>	
								<script type="text/javascript">
								  Calendar.setup(
								    {
								      inputField  : "inicio_${index}",         // ID of the input field
								      ifFormat    : "%d/%m/%Y",    // the date format
								      button      : "iniciobtn_${index}",       // ID of the button
								      showsTime   : false,
								      showsSeconds: false,
								      weekNumbers : false
								    }
								  );
								</script>
							</c:if>
						</n:column>
						<n:column header="T�rmino">
							<t:property name="termino" id="termino_${index}"/>
							<c:if test="${param.ACAO != 'consultar'}">
								<button id="terminobtn_${index}" class="calendarbutton">...</button>	
								<script type="text/javascript">
								  Calendar.setup(
								    {
								      inputField  : "termino_${index}",         // ID of the input field
								      ifFormat    : "%d/%m/%Y",    // the date format
								      button      : "terminobtn_${index}",       // ID of the button
								      showsTime   : false,
								      showsSeconds: false,
								      weekNumbers : false
								    }
								  );
								</script>
							</c:if>
						</n:column>
						<n:column header="Detalhes">
							<a href="javascript:dialog('Aten��o','Aguardem, em breve uma nova funcionalidade! :)')">Mais Informa��es</a>
						</n:column>
						<t:acao>
							<t:property name="id" type="hidden"/>
							<t:property name="cronograma30Passos.id" type="hidden"/>
						</t:acao>
					</t:detalhe>
				</c:if>
			</t:tabelaEntrada>
			<t:tabelaEntrada title="Schedas">
				<br />
				<n:panel colspan="2">
					<a rel="modalFrame" href="/SGP/adm/crud/Scheda?ACAO=criar&showMenu=false" title-modal="Nova Scheda para o Componente ${componente.nome}">
						<button class="tootip-center" title='Clique aqui para criar uma nova Scheda para este componente. Voc� tamb�m poder� vincular esta Scheda � outros componentes.' >Nova Scheda</button>
					</a>
				</n:panel>
				<n:panel colspan="2">
					<br />
				</n:panel>
				<n:panel colspan="2">
					<c:if test="${empty schedasByComponente}">
						<h3>
							<p>N�o Existem Schedas para Este componente!</p>
						</h3>
					</c:if>
					<c:if test="${!empty schedasByComponente}">
						<n:dataGrid itemType="br.com.zatz.sgp.bean.Scheda" var="sc" itens="${schedasByComponente}" cellspacing="0" style="width:150%;"								 
							rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
							rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
							id="tabelaResultados" varIndex="index">
							<t:property name="numero" mode="output"/>
							<n:column header="Descri��o">
								<font style="font-size:12px;"><a href="javascript:void(0)" class="tootip" title="${sc.descricao}">${tldsgp:trunc(sc.descricao,20)}</font>
							</n:column>
							<t:property name="dataSolicitacao" mode="output"/>
							<t:property name="projectChief" mode="output"/>
							<t:property name="status" mode="output"/>
							<n:column header="A��o">
								<a rel="modalFrame" href="/SGP/adm/crud/Scheda?ACAO=editar&showMenu=false&id=${sc.id}" class="tootip" title='Clique aqui para visualizar esta Scheda. Voc� tamb�m poder� adiconar um nova intera��o para esta Scheda.' title-modal="Tela de Edi��o da Scheda do Componente ${componente.nome}">consultar</a>
							</n:column>
						</n:dataGrid>
					</c:if>
				</n:panel>
			</t:tabelaEntrada>
			<t:tabelaEntrada title="Desvios">
				<br />
				<n:panel colspan="2">
					<a rel="modalFrame" href="/SGP/adm/crud/Desvio?ACAO=criar&showMenu=false" title-modal="Novo Desvio para o Componente ${componente.nome}">
						<button class="tootip-center" title='Clique aqui para criar um novo Desvio para este componente. Voc� tamb�m poder� vincular este Desvio � outros componentes.' >Nova Desvio</button>
					</a>
				</n:panel>
				<n:panel colspan="2">
					<br />
				</n:panel>
				<n:panel colspan="2">
					<c:if test="${empty desviosByComponente}">
						<h3>
							<p>N�o Existem Desvios para Este componente!</p>
						</h3>
					</c:if>
					<c:if test="${!empty desviosByComponente}">
						<n:dataGrid itemType="br.com.zatz.sgp.bean.Desvio" var="sc" itens="${desviosByComponente}" cellspacing="0" style="width:150%;" 								 
							rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
							rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
							id="tabelaResultados" varIndex="index">
							<t:property name="numero" mode="output"/>
							<n:column header="Denomina��o">
								<font style="font-size:12px;"><a href="javascript:void(0)" class="tootip" title="${sc.denominacao}">${tldsgp:trunc(sc.denominacao,20)}</font>
							</n:column>
							<t:property name="pendenciaTecnica" mode="output"/>
							<t:property name="provasInterrogativas" mode="output"/>
							<t:property name="autoQualificacao" mode="output"/>
							<t:property name="dimensional" mode="output"/>
							<t:property name="status" mode="output"/>
							<n:column header="A��o">
								<a rel="modalFrame" href="/SGP/adm/crud/Desvio?ACAO=editar&showMenu=false&id=${sc.id}" class="tootip" title='Clique aqui para visualizar este Desvio.' title-modal="Tela de Edi��o do Desvio do Componente ${componente.nome}">consultar</a>
							</n:column>
						</n:dataGrid>
					</c:if>
				</n:panel>
			</t:tabelaEntrada>
		</c:if>
	</t:janelaEntrada>
</t:entrada>


<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 	console.log(${janelaEntrada})
	 	je = '';
	 	je = "${janelaEntrada}";
	 	if(je!=''){
	 		showjanelaEntrada('janelaEntrada_'+je, je, 'janelaEntrada_link_'+je);
	 		if(je!='1' && je!='2' && je!='3')
	 			$('.actionBar').hide();
	 	}
	 });
	 
	 $('#janelaEntrada_link_4,#janelaEntrada_link_5,#janelaEntrada_link_6 ').click(function(){
		 $('.actionBar').hide();
	 });
	 $('#janelaEntrada_link_0,#janelaEntrada_link_1,#janelaEntrada_link_2,#janelaEntrada_link_3').click(function(){
		 $('.actionBar').show();
	 });
	 
	 $(function() {
			$('a[rel=modalFrame]').click(function(e) {
				e.preventDefault();
				var $this = $(this);
				var horizontalPadding = 30;
				var verticalPadding = 30;
		        $('<iframe id="cadScheda" class="externalSite" src="' + this.href + '" />').dialog({
		            title: ($this.attr('title-modal')) ? $this.attr('title-modal') : '',
		            autoOpen: true,
		            width: 980,
		            height: 630,
		            modal: true,
		            resizable: true,
					autoResize: true,
		            overlay: {
		                opacity: 0.5,
		                background: "black"
		            }
		        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
			});
		});
		function closeIframe(){
		   var iframe = document.getElementById('cadScheda');
		   iframe.parentNode.removeChild(iframe);
		   dialog('Aten��o','Aguarde um instante por favor...')
		   if(location.href.indexOf('ACAO') == -1){//significa que � um registro novo
		   		if(location.href.indexOf('Desvio') == -1)
			   		location.href = location.href+"?ACAO=editar&id=${componente.id}&janelaEntrada=5";
			   	else
			   		location.href = location.href+"?ACAO=editar&id=${componente.id}&janelaEntrada=4";
		   }else
		   		if(location.href.indexOf('Desvio') == -1)
		   			location.href = location.href+"&janelaEntrada=5";
		   		else
		   			location.href = location.href+"&janelaEntrada=4";
		}
		function closeIframeWithoutReload(){
			closeIframe();
		}
</script>
<style>
#imagem{
	width: 169px;
	height: 170px;
}
</style>
