<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<c:if test="${empty scheda.id}">
	<t:entrada>
		<t:janelaEntrada submitConfirmationScript="validaEnvio()">
			<t:tabelaEntrada>
				<n:panel >
					<n:panelGrid style="width:100%;" columns="4">
						<input type="hidden" value="${param.showMenu}"  name="showMenu" />
						<t:property name="id" colspan="4"/>
						<t:property name="descricao" style="width:445px;" id="titulo"  colspan="4"/>
						<t:property name="numero" />
						<t:property name="projectChief"/>
						<t:property name="dataSolicitacao" colspan="4"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="projeto" colspan="2"/>
							<t:property name="componente" colspan="2" itens="componenteDAO.findComponentesByProjeto(projeto)"/>
							<t:property name="desenhos" style="width:300px;" includeBlank="false" colspan="3" itens="componenteDAO.findDesenhosByComponentes(componente,projeto)"/>
							<n:panel colspan="1" class="p_dialog_warning">
								<img src="/SGP/img/style/icon-warning.png" >&nbsp;&nbsp;Voc� pode escolher mais de um <br />componente mantendo a tecla<br /> Crtl pressionada, e selecionando atrav�s<br /> do seu mouse!
							</n:panel>
						</n:comboReloadGroup>
						<t:property name="interacoesTransient.detalhes" type="TEXT_AREA" rows="8" cols="80" id="detalhes" colspan="4"/>
						<n:panel colspan="4">
							<div id="informacao-detalhes"></div>
						</n:panel>
						<n:panel colspan="4">
							<t:detalhe name="interacoesTransient.anexos"  labelnovalinha="Anexar Documento">
								<t:property name="anexo"/>
							</t:detalhe>
						</n:panel>
					</n:panelGrid>
				</n:panel>
			</t:tabelaEntrada>
		</t:janelaEntrada>
	</t:entrada>
</c:if>
<c:if test="${!empty scheda.id}">
	<t:entrada >
		<input type="hidden" value="${param.showMenu}"  name="showMenu" />
		<t:property name="id" showLabel="false" label=" " type="hidden"/>
		<br />
		<!-- Exibe o nome, a data q foi criado e o id  	-->
		<t:property name="descricao" label=" "  mode="output"  style="font-size:20px; color: #2281CF;"/>
		<span style="font-size:17px; color:#555;">
			(#<t:property name="numero" mode="output"/>)
		</span>
		<hr style="border: 1px #CDCDCD solid;">
		<font style="color:#999">
			Criada em <font style="color:#E77272"><t:property name="timeInc" pattern="dd/MM/yyyy HH:mm:s" mode="output"/></font>, 
			�ltima altera��o em <font style="color:#E77272"><t:property name="timeAlt" pattern="dd/MM/yyyy HH:mm:s" mode="output"/></font> 
			realizado(a) por <font style="color:#E77272"><t:property name="userAlt" mode="output"/></font>.
		<br />			
			Solicitado pelo cliente <font style="color:#E77272"><b>${cliente}</b></font> em <font style="color:#E77272"><t:property name="dataSolicitacao" mode="output"/></font>
			pelo Project Chief Fiat <font style="color:#E77272"><t:property name="projectChief" mode="output"/></font>.
		<br />
			Esta Scheda � referente ao projeto <font style="color:#E77272">${nomeProjeto}</font> e aos componentes:<font style="color:#E77272">${nomeComponentes}</font>
		
		</font>
				
		<!-- exibe o cabecalho com status, departamento e o criador do scheda -->
		<div id="corpo-interecao">	
			<div class="corpo-interecao-cabecalho">
				<div class="corpo-interecao-cabecalho-status">
					<b>Status</b><br/>
					<c:if test="${scheda.status.id != 3}">
						<t:property name="status" style="border:3px solid #82CFFA;" id="status" onchange="verificaStatus(this.value)"/>
					</c:if>
					<c:if test="${scheda.status.id == 3}">
						<t:property name="status" mode="output"/>
					</c:if>
				</div>
				<div class="corpo-interecao-cabecalho-criado">
					<b>Criada por: </b>${scheda.userInc }
				</div>
				<div class="corpo-interecao-cabecalho-anexo">
					<b>Anexos</b>
				</div>
			</div>
					
			<!-- Loop para exibic�o das intera��es do scheda -->
			<t:detalhe name="interacoes"  showBotaoNovaLinha="false" showBotaoRemover="false" showColunaAcao="false"  >
				<n:column  style="min-width:100px; max-width:100px; min-height:400px; text-align:center;  border: 1px #CCC solid;">
					 <b><t:property name="userInc" mode="output"/></b><br/>
					 <span class="user">
					 		Usu�rio<br/><br/>
					 		<t:property name="timeInc" mode="output"/><br/>
					</span>
					
				</n:column>
				<n:column  style="min-width:400px; max-width:400px; min-height:400px; text-align:justify; border: 1px #CCC solid; padding-left: 20px; padding-right: 20px; padding-top: 20px;">	
					<t:property name="detalhes"  mode="output"/><br/>
				</n:column>	
				<n:column style="min-width:140px; max-width:140px;  min-height:400px; text-align:center;  border: 1px #CCC solid; font-size:10px;">
					<n:panel id="interacao-anexo">
						<c:forEach items="${interacoes.anexos}" var="an">
							<b style="font-size:14px;"><a href="/SGP/DOWNLOADFILE/${an.anexo.cdfile}" class="filelink"><img src="/SGP/img/style/download.png"/>${an.anexo.name}</a><br/></b>
						</c:forEach>
					</n:panel>	
				</n:column>
			</t:detalhe>
		
			<!-- Exibe um campo para nova intere��o em rela��o a scheda -->
			<c:if test="${scheda.status.id != 3}">
				<n:panel>	
					<table class="tabela-nova-interacao">
						<tr>
							<td>
								<b>Nova Intera��o:</b>
							</td>
							<td>
								<t:property  name="interacoesTransient.detalhes" type="TEXT_AREA" rows="6" cols="70"  id="detalhesInteracao"/>
							</td>
						</tr>
						<tr style="display:none;" id="tr_investimento">
							<td>
								<b>Investimento</b>
							</td>
							<td>
								<t:property  name="interacoesTransient.investimento" id="investimento"/><span class="requiredMark">*</span>
							</td>
						</tr>
						
						<tr>
							<td>
							</td>
							<td>
								<div id="error-detalhes" ></div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<t:detalhe name="interacoesTransient.anexos" labelnovalinha="Anexar Documento">
									<n:column header="Anexo">
										<t:property name="anexo"/>
									</n:column>
								</t:detalhe>
							</td>
						</tr>
					</table>
					<hr style="border:1px solid #CDCDCD">	
						<button type="button" class="button-salvar" onclick="form.ACAO.value ='salvar';if (validaEnvioInteracao()) {dialogAguarde();form.action = '/SGP/adm/crud/Scheda'; form.validate = 'true'; submitForm()}">Salvar</button>
				</n:panel>	
			</c:if>
			<c:if test="${scheda.status.id == 3}">
				<n:submit action="reabrir" onclick="dialogAguarde();" parameters="id=${scheda.id}">Reabrir Scheda</n:submit>
			</c:if>
		</div>			
	</t:entrada>
</c:if>
<script type="text/javascript">

$(document).ready(function(){
	$('#last-update').hide();
});

function validaEnvioInteracao(){
	detalhes = $('#detalhesInteracao').val();
	
	if(detalhes.length <= 20 || detalhes.length > 255){
		$('#error-detalhes').html("Os detalhes devem ter no m�nimo 20 caracteres e no m�ximo 255.");
		$('#error-detalhes').slideDown();
		 $('#detalhesInteracao').focus();
		
		return false
	} else{
		$('#error-detalhes').slideUp();
	}
	investimento = $('#investimento').val();
	status = $('#status').val();
	if(status == "br.com.zatz.sgp.bean.Status[id=3]"){
		if(investimento == ''){
			dialog('Aten��o','Quando uma Scheda � fechada, um valor do investimento deve ser informado! Por favor, informe o valor do investimento e clique em salvar!');
			return false;
		}
	}
	return true;
}

function validaEnvio(){

	detalhes = $('#detalhes').val();	
	
	if(detalhes.length <= 20 || detalhes.length > 255){
		$('#informacao-detalhes').html("Os detalhes devem ter no m�nimo 20 caracteres e no m�ximo 255.");
		$('#informacao-detalhes').slideDown();
		return false
	} else{
		$('#informacao-detalhes').slideUp();
	}
	investimento = $('#investimento').val();
	status = $('#status').val();
	if(status == "br.com.zatz.sgp.bean.Status[id=3]"){
		if(investimento == ''){
			dialog('Aten��o','Quando uma Scheda � fechada, um valor do investimento deve ser informado! Por favor, informe o valor do investimento e clique em salvar!');
			return false;
		}
	}
	
	return true;
}

function verificaStatus(param){
	console.log(param)
	if(param == "br.com.zatz.sgp.bean.Status[id=3]")
		$('#tr_investimento').fadeIn();
	else
		$('#tr_investimento').hide();
}

</script>