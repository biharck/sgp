<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<div class="ultimaalteracao">
	<h6 class="last-login">
		Bem Vindo: 
		
		<font style="color:#E77272">
			<c:out value="${nomeExibicao}"/>
		</font> 
		
		seu �ltimo login foi dia 
		
		<font style="color:#E77272">
			<c:out value="${dtUltimoLogin}"/>
		</font>
	</h6>
</div>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="nome" colspan="4" style="width: 400px;"/>
					<t:property name="identificacao" colspan="2"/>
					<t:property name="cliente" colspan="2"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false" showEditarLink="false">
			<t:property name="nome"/>
			<t:property name="identificacao"/>
			<t:property name="cliente"/>
			<t:property name="gerente"/>
			<t:property name="preVP" />
			<t:property name="verificaProcesso"/>
			<t:property name="preSerie"/>
			<t:property name="sop"/>
			<t:acao>
				<n:link class="tootip" url="/adm/crud/Componente" action="doListagem" parameters="idProjeto=${projeto.id}" description='Seleciona o projeto para listar todos os componentes daquele projeto'  onclick="dialogAguarde();">selecionar</n:link>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
