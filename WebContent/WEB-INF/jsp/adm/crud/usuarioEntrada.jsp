<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada submitConfirmationScript="validaIgualdade()">
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false"/>
					<t:property name="nome" id="nome" colspan="4" style="width:400px" renderAs="doubleline"/>
					
					<t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
					<c:if test="${empty usuario.id}">
						<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
					</c:if>
					
					<t:property name="login" colspan="2" style="width:400px" renderAs="doubleline"/>
					<c:if test="${empty usuario.id}">
						<t:property name="senha"   renderAs="doubleline" id="senha" onchange="validaTamMinimoCampo($('#senha'),'senha',4)"/>
						<t:property name="reSenha" renderAs="doubleline" id="resenha" onchange="validaIgualdadeCampo($('#senha'),this,'senha')"/>
					</c:if>
					
					<t:property name="ativo" renderAs="doubleline"/>
					<t:property name="senhaProvisoria" renderAs="doubleline" class="tootip" title="No pr�ximo acesso dever� ocorrer altera��o de senha"/>
					
					<n:panel valign="top" style="padding-top:4px" renderAs="doubleline">Papeis<span class="requiredMark">*</span></n:panel>
					<n:dataGrid itemType="br.com.zatz.sgp.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
						<n:column width="20">
		                    <n:input name="papeis"  value="${row}" type="checklist" itens="${usuario.papeis}"/>
		                </n:column>
		                <n:column>
			                <t:property name="nome" mode="output"/>
		                </n:column>
					</n:dataGrid>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>


<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });

	 function validaIgualdade(){
        edicao = ${empty usuario.id};
	 	if(edicao){
	 		return(validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail') &&
                   validaIgualdadeCampo($('#senha'),$('#resenha'),'senha') &&
                   validaTamMinimoCampo($('#senha'),'senha',4));
	 	}else return true;
	 }
</script>