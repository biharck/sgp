<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="nome" colspan="4" style="width: 400px;"/>
					<t:property name="numero" colspan="2"/>
					<t:property name="matricula" colspan="2"/>
					<t:property name="numeroTCAE" colspan="2"/>
					<t:property name="projeto" colspan="2" disabled="disabled"/>
					<t:property name="projectChief" colspan="2"/>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false" showEditarLink="false" showConsultarLink="false">
			<t:property name="nome" />
			<t:property name="numero" />
			<t:property name="matricula" />
			<t:property name="numeroTCAE"/>
			<t:property name="projectChief" />
			<t:property name="projeto" />
			<t:property name="massa" />
			<t:acao>
				<n:link action="consultar" class="consult tootip" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" onclick="dialogAguarde();" description="Selecionar o Componente." >consultar</n:link>
				<c:if test="${empty componente.vdVt}">
					<a onclick="dialogAguarde();" description="Clique aqui para adicionar um novo componente baseado nas informa��es deste! lembre-se que o vd/vt ser� este componente por padr�o!." href="/SGP/adm/crud/Componente?ACAO=copiar&id=${componente.id}">copiar</a>
				</c:if>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
