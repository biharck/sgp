<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada title="Descri��o do Projeto">
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false"/>
					<t:property name="nome" id="nome" colspan="2"  renderAs="doubleline" />
					<t:property name="identificacao" colspan="2" renderAs="doubleline"/>
					<t:property name="cliente" colspan="2" renderAs="doubleline"/>
					<t:property name="gerente" colspan="2" renderAs="doubleline" insertPath="/adm/crud/Usuario"/>
					<t:property name="descricao" colspan="4" cols="30" rows="5" style="width:400px" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>	
		</t:tabelaEntrada>
		<t:tabelaEntrada title="Datas Chave">
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="preVP" colspan="2"  renderAs="doubleline" />
					<t:property name="verificaProcesso" colspan="2" renderAs="doubleline"/>
					<t:property name="preSerie" colspan="2" renderAs="doubleline"/>
					<t:property name="sop" colspan="2" renderAs="doubleline" />
				</n:panelGrid>
			</n:panel>	
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>


<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>

