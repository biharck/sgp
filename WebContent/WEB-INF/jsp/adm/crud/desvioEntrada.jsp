<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<input type="hidden" value="${param.showMenu}"  name="showMenu" />
			<n:panel >
				<n:panelGrid columns="4">
					
					<t:property name="id" colspan="4"/>
					
					<t:property name="denominacao" style="width:400px;" id="titulo"  colspan="4"/>
					
					<t:property name="numero" colspan="4" />
					
					<c:if test="${empty desvio.id}">
						<n:comboReloadGroup useAjax="true">
							<t:property name="projeto" colspan="2"/>
							<t:property name="componente" colspan="2" itens="componenteDAO.findComponentesByProjeto(projeto)"/>
							
							<t:property name="desenhos" style="width:300px;" includeBlank="false" colspan="3" itens="componenteDAO.findDesenhosByComponentes(componente,projeto)"/>
							<n:panel colspan="1" class="p_dialog_warning">
								<img src="/SGP/img/style/icon-warning.png" >&nbsp;&nbsp;Voc� pode escolher mais de um <br />componente mantendo a tecla<br /> Crtl pressionada, e selecionando atrav�s<br /> do seu mouse!
							</n:panel>
						</n:comboReloadGroup>
					</c:if>
					<c:if test="${!empty desvio.id}">
						<n:comboReloadGroup useAjax="true">
							<t:property name="projeto" colspan="2" disabled="disabled"/>
							<t:property name="componente" colspan="2" disabled="disabled" itens="componenteDAO.findComponentesByProjeto(projeto)"/>
							<n:panel colspan="3">
								Desenhos: 
								<c:forEach items="${desvio.desenhos}" var="des" >
									<br />${des.numero}
								</c:forEach>
							</n:panel>
							<n:panel colspan="1" class="p_dialog_warning">
								<img src="/SGP/img/style/icon-warning.png" >&nbsp;&nbsp;Voc� pode escolher mais de um <br />componente mantendo a tecla<br /> Crtl pressionada, e selecionando atrav�s<br /> do seu mouse!
							</n:panel>
						</n:comboReloadGroup>
					</c:if>
					
					<n:group legend="Pend�ncias T�cnicas" colspan="2">
						<t:property name="pendenciaTecnica"  colspan="4"/>
						<t:property name="numeroPendenciaTecnica" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoPendenciaTecnica"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Provas Interrogativas" colspan="2">
						<t:property name="provasInterrogativas" colspan="4"/>
						<t:property name="numeroProvasInterrogativas" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoProvasInterrogativas"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Auto Qualifica��o" colspan="2">
						<t:property name="autoQualificacao" colspan="4"/>
						<t:property name="numeroAutoQualificacao" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoAutoQualificacao"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Dimensional" colspan="2">
						<t:property name="dimensional" colspan="4"/>
						<t:property name="numeroDimensional" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoDimensional"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Funcional. Confiab." colspan="2">
						<t:property name="funcionalConfiab" colspan="4"/>
						<t:property name="numeroFuncionalConfiab" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoFuncionalConfiab"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Est�tico" colspan="2">
						<t:property name="estetico" colspan="4"/>
						<t:property name="numeroEstetico" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoEstetico"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Material" colspan="2">
						<t:property name="material" colspan="4"/>
						<t:property name="numeroMaterial" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoMaterial"/>	
						</n:panel>
					</n:group>
					
					<n:group legend="Proc. Fornec." colspan="2">
						<t:property name="procFornec" colspan="4"/>
						<t:property name="numeroProcFornec" colspan="2"/>
						<n:panel colspan="2">
							Anexo:<t:property name="anexoProcFornec"/>	
						</n:panel>
					</n:group>
					
					
					<t:property name="motivoDesvio" type="TEXT_AREA" rows="8" cols="80" colspan="4"/>
					<t:property name="causaDesvio" type="TEXT_AREA" rows="8" cols="80" colspan="4"/>
					<t:property name="planoAcao" type="TEXT_AREA" rows="8" cols="80" colspan="4"/>
					<t:property name="inicioLimiteAceito"/>
					<t:property name="fimLimiteAceito"/>
					<t:property name="qtdLimitePecas"/>
					<t:property name="rastreabilidade"/>
					<t:property name="validadoEQF"/>
					<t:property name="amostraEntregue"/>
					<t:property name="benestare"/>
					<n:panel colspan="2"></n:panel>
					
					<n:group legend="Proc. Fornec." colspan="4">
						<t:property name="status" colspan="4"/>
						<t:property name="numeroStatus"/>
						<n:panel colspan="2">					
							Anexo:	<t:property name="anexoStatus"/>
						</n:panel>
					</n:group>
					
					
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
