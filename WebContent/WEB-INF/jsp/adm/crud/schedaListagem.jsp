<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem >
	<t:janelaFiltro>
		<t:tabelaFiltro columns="4">
			<t:property name="descricao" size="40px;"  />
			<t:property name="status" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showConsultarLink="false" showEditarLink="false" showExcluirLink="false">
			<t:property name="numero"/>
			<t:property name="descricao" label="T�tulo"/>
			<n:column header="Cria��o">
				<t:property name="timeInc" mode="output"/>
			</n:column>
			<n:column header="Altera��o">
				<t:property name="timeAlt" mode="output"/>
			</n:column>
			<t:property name="status"/>
			<t:acao>
				<n:link action="editar" parameters="id=${scheda.id}">consultar</n:link>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function(){
	$('.resultWindow table tbody tr').mouseover(function(){
		$(this).addClass("destacarLinha");
	})
	$('.resultWindow table tbody tr').mouseout(function(){
		$(this).removeClass("destacarLinha");
	})
})	
	
</script>