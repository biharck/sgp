
<span style="font-size:20px; color: #2281CF;">Prezado(a) {projectChief},</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">
	A Scheda de n&uacute;mero <font style="color:#E77272">{numero}</font> relacionada ao componente <br />
	<font style="color:#1F75CC;"> 
		{componente}
	</font>
	que est&aacute; vinculado ao projeto 
	<font style="color:#1F75CC;"> 
		{projeto}
	</font> foi fechada. <br />

<br/ >
<b>Autor do Fechamento da Scheda:</b>
<font style="color:#1F75CC;"> 
	{autor}
</font>
<br />
<b>Intera&ccedil;&atilde;o:</b>
<font style="color:#1F75CC;"> 
	{interacao}
</font>
<br />
<br />
<b>Investimento:</b>
<font style="color:#1F75CC;"> 
	R${investimento}
</font>
<br />
<br />
Para acessar o sistema SGP,  <a href="{link}">Clique Aqui</a> e voc&ecirc; ser&aacute; redirecionado.<br>
<br>
<br>
<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="http://www.zatz.eng.br/images/logo.png" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="http://www.zatz.eng.br" target="_blank">www.zatz.eng.br</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>

