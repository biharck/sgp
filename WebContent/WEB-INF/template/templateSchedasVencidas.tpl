<h3><font style="color:#E77272">Aten&ccedil;&atilde;o</font></h3>
<span style="font-size:20px; color: #2281CF;">Prezado(a) {projectChief},</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">	
	A Scheda de n&uacute;mero <font style="color:#E77272">{numero}</font> est&aacute; <font style="color:#E77272">Vencida</font>. <br />	
	Esta Scheda envolve o componente:<br />
	<font style="color:#1F75CC;"> 
		{componente}
	</font>
	que est&aacute; vinculado ao projeto 
	<font style="color:#1F75CC;"> 
		{projeto}
	</font>. <br />

&Eacute; de extrema import&acirc;ncia que o senhor(a) acompanhe o andamento desta Scheda que venceu <br />
no dia <font style="color:#E77272">{vencimento}</font>.<br/ > 
Por favor, entre em contato com seu cliente {cliente} para uma posi&ccedil;&atilde;o.
<br>
Para acessar o sistema SGP,  <a href="{link}">Clique Aqui</a> e voc&ecirc; ser&aacute; redirecionado.<br>
<br>
<br>
<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="http://www.zatz.eng.br/images/logo.png" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="http://www.zatz.eng.br" target="_blank">www.zatz.eng.br</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>

