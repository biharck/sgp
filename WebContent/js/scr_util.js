function scr_util(){
	var selectedIndex = null;
} 

/**
 * Acessa o link que tem a classe activation
 */
scr_util.prototype.editarRegistro = function (obj){
	var acv = $(".consult",obj);
	if(acv.size() > 0)
		window.location = $(".consult",obj).attr("href");
	else
		closeDialog();
}

/**
 * Colore a linha que est� o mouse no DG
 */
scr_util.prototype.coloreLinha = function (tabelaId,elementoSelecionado){
	
	var tabela = document.getElementById(tabelaId);
	var cellRows = tabela.rows;

	$('.dataGrid tbody tr td a').each(function(){$(this).css('color','#1F75CC')})	
	for(i = 0; i< cellRows.length ; i++){
		cellRows[i].style.backgroundColor = "";
		cellRows[i].style.color = "#555";
	}
	
	elementoSelecionado.style.backgroundColor = "#388EC8";
	elementoSelecionado.style.color = "#fff";
	
	ultimaTD = cellRows[elementoSelecionado.rowIndex].children.length-1;
	
	for(i = 0; i< cellRows[elementoSelecionado.rowIndex].children[ultimaTD].children.length ; i++){
		cellRows[elementoSelecionado.rowIndex].children[ultimaTD].children[i].style.color = "#fff";
	}
	
	this.selectedIndex = elementoSelecionado.rowIndex;
}



/**
 * Controla o evento de entrada
 */
scr_util.prototype.mouseonOverTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex)
		elementoSelecionado.style.backgroundColor = '#F0F9FF';
}
/**
 * Controla o evento de sa�da
 */
scr_util.prototype.mouseonOutTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex){
		elementoSelecionado.style.backgroundColor = '';
	}
	//elementoSelecionado.children[0].children[1].style.display =  "none";
}

scr_util.prototype.getChecksSelecionados = function(){
	var check = $("#allInputs").attr("checked");
	
	$("#tabelaResultados input[type=checkbox][name=radioselecionado]").each(function(){
		if(check) $(this).attr("checked",check);
		else $(this).removeAttr("checked");
	});
}

scr_util.prototype.getSelectedValues = function(){
	var selectedValues = "";
	
	
	$("#tabelaResultados input[type=checkbox][name=radioselecionado]").each(function(){
		if(this.checked) selectedValues += this.value+","; 
	});
	
	if(selectedValues != ""){
		selectedValues = selectedValues.substr(0,(selectedValues.length -1));
	}
	return selectedValues;
}

scr_util.prototype.validarChecksSelecionados = function(){
		openDialogInformation("Para realizar a exclus�o de um registro, voc� deve selecionar os registros que deseja excluir antes.");
		return false;
}

scr_util.prototype.clearForm = function(){
	
	$('.backGroundFiltro input[type=text]').each(function(){
		$(this).val('');
	});
	$('.backGroundFiltro input[type=date]').each(function(){
		$(this).val('');
	});
	$('.backGroundFiltro select').each(function(){
		$(this).val('<null>');
	});
}


var $csu = new scr_util();